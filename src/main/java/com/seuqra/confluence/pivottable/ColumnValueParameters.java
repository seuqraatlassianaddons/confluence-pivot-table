package com.seuqra.confluence.pivottable;

import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.atlassian.sal.api.message.I18nResolver;

public class ColumnValueParameters {
	private static Log log = LogFactory.getLog("com.seuqra.confluence.pivottable");
	private String replacement = null;
	private String regexp = null;
	private String color = null;
	
	public ColumnValueParameters(String parameters, I18nResolver i18n) {
		parse(parameters, i18n);			
	}
	
	public ColumnValueParameters(String parameters) {
		this(parameters, (I18nResolver) null);			
	}
	
	public ColumnValueParameters(String regexp, String replacement) {
		this.regexp = regexp;
		this.replacement = replacement;
	}
	
    private void parse(String parameters, I18nResolver i18n) {
		if (parameters == null) {
			return;
		}
		StringTokenizer params = new StringTokenizer(parameters , "/", true);
		String param = null;
		int currentParam = 0;
		while (params.hasMoreTokens()) {
			param = params.nextToken();
			if ("/".equals(param)) {
				currentParam++;
				continue;
			}
			switch (currentParam) {
			case 0:
				replacement = param;
				break;				
			case 1:
				regexp = param;
				break;
			case 2:
				color = param;
				break;
			}
		}
		if (regexp == null) {
			regexp = replacement;
		}
	}
    
    /**
     * @return the string to be substituted
     */
    public String getReplacement() {
    	return replacement;
    }
    /**
     * @return the regular expression to which this string is to be matched
     */
    public String getRegexp() {
    	return regexp;
    }
    /**
     * @return the color
     */
    public String getColor() {
    	return color;
    }
    
    public boolean matches(String s) {
    	if (s == null) {
    		return false;
    	}
    	return (regexp == null?true:s.matches(regexp));
    }
    
    public String getReplacement(String toReplace) {
    	//debug(toReplace);
    	try {
    		if (matches(toReplace)) {
    			return toReplace.replaceFirst(regexp, replacement);
    		} else {
    			log.debug("NO MATCH");
    		}
    	} catch (Exception e) {
    	}
		return toReplace;
    }
    
	private void debug(String toReplace) {
    	log.debug("ColumnValueParameters.getReplacement() toReplace="+toReplace);
    	log.debug("ColumnValueParameters.getReplacement() regexp="+regexp);
    	log.debug("ColumnValueParameters.getReplacement() replacement="+replacement);
    	if (regexp == null || replacement == null || toReplace == null) return;
		Pattern p = Pattern.compile(regexp);
		Matcher m = p.matcher(toReplace);
		// launch the search on all the occurences
		boolean b = m.matches();
		// If it matches
		if(b) {
		    // For each group
		    for(int i=0; i <= m.groupCount(); i++) {
		    	log.debug("Group " + i + " : " + m.group(i));
		    }
		} else {
			log.debug("NO MATCH");
		}
	}
}
