package com.seuqra.confluence.pivottable;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.atlassian.bandana.BandanaManager;
import com.atlassian.confluence.renderer.UserMacroConfig;
import com.atlassian.sal.api.message.I18nResolver;

/**
 * A resource of message.
 */
@Path("/usermacros")
public class UserMacroConfigResource {
	private I18nResolver i18n;
	private BandanaManager bandanaManager;
	
	public UserMacroConfigResource(BandanaManager bandanaManager, I18nResolver i18n) {
		this.i18n = i18n;
		this.bandanaManager = bandanaManager;
	}

    @GET @Path("{macroId}")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getMessage(@PathParam("macroId") String macroId) {
    	// For security reason, return only info on user macro PIVOT_TABLE_USER_MACRO_PIVOT_VALUES_RENDERING
    	if (PivotTable.PIVOT_TABLE_USER_MACRO_PIVOT_VALUES_RENDERING.equals(macroId)) {
			UserMacroConfig userMacroConfig = Utils.getUserMacroConfig(bandanaManager, macroId);
			if (userMacroConfig != null) {
				return Response.ok(new UserMacroConfigResourceModel(userMacroConfig, i18n)).build();
			} else {
				return Response.status(Status.NOT_FOUND).build();
			}
    	}
		return Response.status(Status.UNAUTHORIZED).build();
    }
}
