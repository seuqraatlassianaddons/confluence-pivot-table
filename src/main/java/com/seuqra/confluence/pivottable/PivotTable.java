package com.seuqra.confluence.pivottable;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Entities.EscapeMode;
import org.jsoup.parser.Tag;
import org.jsoup.select.Elements;

import com.atlassian.bandana.BandanaManager;
import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.sal.api.message.I18nResolver;

public class PivotTable implements Macro {
	
	private static Log log = LogFactory.getLog("com.seuqra.confluence.pivottable");
	private I18nResolver i18n;
	private BandanaManager bandanaManager;
	
	static final String PARAM_COLUMN_NAME_FOR_VALUES = "columnNameForValues";
	static final String PARAM_REGEXP_FOR_VALUES = "regexpForValues";
	static final String PARAM_REPLACEMENT_FOR_VALUES = "replacementForValues";
	static final String PARAM_COLUMN_NAME_FOR_ROWS = "columnNameForRows";
	static final String PARAM_LABELS_AND_REGEXPS_FOR_ROWS = "labelsAndRegExpsForRows";
	static final String PARAM_TYPE_OF_CALCULATION = "typeOfCalculation";
	public static final String PARAM_TYPE_OF_CALCULATION_VALUE_COUNT = "count";
	public static final String PARAM_TYPE_OF_CALCULATION_VALUE_SUM = "sum";
	public static final String PARAM_TYPE_OF_CALCULATION_VALUE_AVERAGE = "average";
	static final String PARAM_COLUMN_FILTER_NAME = "columnFilterName";
	static final String PARAM_DISPLAY_EMPTY_COLUMNS = "displayEmptyColumns";
	static final String PARAM_COLUMN_NAME_FOR_COLUMNS = "columnNameForColumns";
	static final String PARAM_LABELS_AND_REGEXPS_FOR_COLUMNS = "labelsAndRegExpsForColumns";
	static final String PARAM_DISPLAY_TOTAL_FOR_ROWS = "displayTotalForRows";
	static final String PARAM_DISPLAY_TOTAL_FOR_COLUMNS = "displayTotalForColumns";
	static final String PARAM_DISPLAY_BODY_TABLES = "displayBodyTables";
	static final String PARAM_DISPLAY_BODY_TABLES_BEFORE = "before";
	static final String PARAM_DISPLAY_BODY_TABLES_AFTER = "after";
	
	static final ColumnValueParameters COLUMN_VALUE_WITHOUT_ROW = new ColumnValueParameters("ALL/.*");
	static final String PIVOT_TABLE_USER_MACRO_PIVOT_VALUES_RENDERING = "pivot-values-rendering";
	static final String PIVOT_TABLE_USER_MACRO_PARAM_BODY_TABLES_VALUES = "bodyTableValues";
	static final String PIVOT_TABLE_USER_MACRO_PARAM_TYPE_OF_RENDERING = "typeOfRendering";
	
    public PivotTable(BandanaManager bandanaManager, I18nResolver i18n) {
    	this.bandanaManager = bandanaManager;
        this.i18n = i18n;
    }

    @Override
    public String execute(Map<String, String> parameters, String body, ConversionContext context) throws MacroExecutionException {
    	try {
    		return executeWithI18n(parameters, body, context, i18n);
    	} catch (Throwable e) {
    		log.error("Cannot execute macro", e);
    	}
    	return "";
    }
    
    String executeWithI18n(Map<String, String> parameters, String body, ConversionContext context, I18nResolver i18n) throws MacroExecutionException {
    	ColumnValueParameters regexpForValues = new ColumnValueParameters(parameters.get(PARAM_REGEXP_FOR_VALUES),parameters.get(PARAM_REPLACEMENT_FOR_VALUES));			
    	ColumnValueParameters columnFilterParameters = new ColumnValueParameters(parameters.get(PARAM_COLUMN_FILTER_NAME));			
    	ColumnParameters columnParametersForRows = new ColumnParameters(parameters.get(PARAM_LABELS_AND_REGEXPS_FOR_ROWS), i18n);
    	ColumnParameters columnParametersForColumns = null;
    	//boolean displayEmptyValues = Boolean.parseBoolean(parameters.get(PARAM_DISPLAY_EMPTY_COLUMNS));
    	boolean displayTotalForRows = Boolean.parseBoolean(parameters.get(PARAM_DISPLAY_TOTAL_FOR_ROWS));
    	boolean displayTotalForColumns = Boolean.parseBoolean(parameters.get(PARAM_DISPLAY_TOTAL_FOR_COLUMNS));
    	String columnNameForValues = parameters.get(PARAM_COLUMN_NAME_FOR_VALUES);
    	String columnNameForRows = parameters.get(PARAM_COLUMN_NAME_FOR_ROWS);
    	String columnNameForColumns = parameters.get(PARAM_COLUMN_NAME_FOR_COLUMNS);
    	String typeOfCalculation = parameters.get(PARAM_TYPE_OF_CALCULATION);
    	String totalForColumnsLabel = null;
    	String totalForRowsLabel = null;
    	String calculationForRowsLabel = null;
    	
    	if (columnNameForRows != null && displayTotalForColumns) {
    		totalForColumnsLabel = i18n.getText("com.seuqra.confluence.pivot-table.total");
    	}
    	columnParametersForColumns = new ColumnParameters(parameters.get(PARAM_LABELS_AND_REGEXPS_FOR_COLUMNS), i18n);
    	if (columnNameForColumns != null && displayTotalForRows) {
    		totalForRowsLabel = i18n.getText("com.seuqra.confluence.pivot-table.total");
    	} 
    	
    	String dataDisplay = parameters.get(PARAM_DISPLAY_BODY_TABLES);
    	Document doc = Jsoup.parseBodyFragment(body);
    	doc.outputSettings().escapeMode(EscapeMode.xhtml);
    	Element htmlBody = doc.body();
    	Elements bodyTables = htmlBody.select("table");
    	if (bodyTables.size() == 0) {
    		return "<p>"+i18n.getText("com.seuqra.confluence.pivot-table.error.noBodyTable")+"</p>";
    	}
    	
    	ListIterator<Element> tableList = bodyTables.listIterator();
    	Map<String, Map<String, List<String>>> rowsAndColumnsMap = columnParametersForColumns.initPivotMapWithEmptyValues();
    	Map<String, List<String>> rowsMap = columnParametersForRows.initMapWithEmptyValues();
    	Map<String, List<String>> columnsMap = columnParametersForColumns.initMapWithEmptyValues();
    	while (tableList.hasNext()) {
    		Element table = tableList.next();
    		Elements trs = table.select("tr");
    		int columnFilterPosition = Utils.columnPosition(columnFilterParameters.getReplacement(), table);
    		
    		int columnPositionForValues = Utils.columnPosition(columnNameForValues, table);
    		if (columnPositionForValues < 0) {
    			columnPositionForValues = 0;
    			columnNameForValues = Utils.getTdText(0, table.select("th"));
			}
    		int columnPositionForRows = columnPositionForValues;
    		if (columnNameForRows != null) {
    			columnPositionForRows = Utils.columnPosition(columnNameForRows, table);
	    		if (columnPositionForRows < 0) {
					return "<p>"+i18n.getText("com.seuqra.confluence.pivot-table.error.wrongColumn", columnNameForRows)+"</p>";
				}
    		}
    		int columnPositionForColumns = -1;
    		if (columnNameForColumns != null) {
    			columnPositionForColumns = Utils.columnPosition(columnNameForColumns, table);
        		if (columnPositionForColumns < 0) {
    				return "<p>"+i18n.getText("com.seuqra.confluence.pivot-table.error.wrongColumn", columnNameForColumns)+"</p>";
    			}
    		}
    		
    		// If no column Name has been set, the only column of the Pivot Table is the values calculation.
    		// The Label of this column depends on the type of calculation
    		if (PARAM_TYPE_OF_CALCULATION_VALUE_COUNT.equals(typeOfCalculation)) {
    			calculationForRowsLabel = i18n.getText("com.seuqra.confluence.pivot-table.count", columnNameForValues);
    		} else if (PARAM_TYPE_OF_CALCULATION_VALUE_SUM.equals(typeOfCalculation)) {
    			calculationForRowsLabel = i18n.getText("com.seuqra.confluence.pivot-table.sum", columnNameForValues);
    		} else if (PARAM_TYPE_OF_CALCULATION_VALUE_AVERAGE.equals(typeOfCalculation)) {
    			calculationForRowsLabel = i18n.getText("com.seuqra.confluence.pivot-table.average", columnNameForValues);
    		} else {
    			calculationForRowsLabel = columnNameForValues;
    		}
    		
    		for (Element tr : trs) {
    			Elements tds = tr.select("td");
    			if (columnPositionForRows < tds.size()) {
    				String tdFilterValue = Utils.getTdText(columnFilterPosition, tds);
    				if ((tdFilterValue == null) || columnFilterParameters.matches(tdFilterValue)) {
    					Element tdForRows = tds.get(columnPositionForRows);
    					String rowTdValue = Utils.getTdText(columnPositionForRows, tds, i18n.getText("com.seuqra.confluence.pivot-table.blanks"));
    					ColumnValueParameters rowColumnParameters = columnParametersForRows.getColumnParametersForPatternMatching(rowTdValue);
    					if (columnNameForRows == null) {
    						// See https://bitbucket.org/seuqra/confluence-pivot-table/issues/6/no-empty-options-in-values-columns-and
    						// Create a ColumnValueParameters with the value of calculationForRowsLabel and a regExp to retrieve all the values
    						rowColumnParameters = new ColumnValueParameters("/.*", calculationForRowsLabel);
    					}
    					String value = Utils.getTdText(columnPositionForValues, tds);

    					Utils.addValueInMap(rowColumnParameters, tdForRows, value, rowsMap);
    					if (columnPositionForColumns != -1) {
        					Element tdForColumns = tds.get(columnPositionForColumns);
    						// Remove space and &nbsp;
    						String columnTdValue = Utils.getTdText(columnPositionForColumns, tds, i18n.getText("com.seuqra.confluence.pivot-table.blanks"));

    						ColumnValueParameters columnColumnParameters = columnParametersForColumns.getColumnParametersForPatternMatching(columnTdValue);
        					Utils.addValueInMap(columnColumnParameters, tdForColumns, value, columnsMap);
    						Utils.addValueInMapOfMap(rowColumnParameters, columnColumnParameters, value, rowsAndColumnsMap);
    					}
    				}
    			}
    		}    		
    	}
    	
    	if (columnNameForColumns == null) {
    		rowsAndColumnsMap.put(calculationForRowsLabel, rowsMap);
    	}
    	return buildTable(rowsAndColumnsMap, rowsMap, columnsMap, columnNameForRows, columnNameForColumns, totalForRowsLabel, totalForColumnsLabel, dataDisplay, htmlBody.children(), displayTotalForColumns, typeOfCalculation, calculationForRowsLabel, regexpForValues, context);
    }
    
    String buildTable(Map<String, Map<String, List<String>>> rowsAndColumnsMap, Map<String, List<String>> rowsMap, Map<String, List<String>> columnsMap, String columnNameForRows, String columnNameForColumns, String totalForRowsLabel, String totalForColumnsLabel, String dataDisplay, Elements elements, boolean displayTotalForColumns, String typeOfCalculation, String calculationForRowsLabel, ColumnValueParameters regexpForValues, ConversionContext context) {
    	StringBuffer result = new StringBuffer();
    	if (PARAM_DISPLAY_BODY_TABLES_BEFORE.equals(dataDisplay)) {
    		result.append(elements);
    	}		
    	// See https://bitbucket.org/seuqra/confluence-pivot-table/issues/6/no-empty-options-in-values-columns-and
    	// If columnNameForRows is null, pass columnNameForColumns for the upper left cell label
    	List<List<String>> list = buildPivotTable(rowsAndColumnsMap, rowsMap, columnNameForRows != null ? columnNameForRows:columnNameForColumns, typeOfCalculation, regexpForValues, context);
    	addGrandTotals(list, rowsMap, columnsMap, totalForColumnsLabel, totalForRowsLabel, typeOfCalculation, regexpForValues, context);
    	updateUpperLeftLabel(list, columnNameForRows, columnNameForColumns, calculationForRowsLabel);
    	/*
    	Cannot use Velocity because it escapes the XML macro content 
    	Map<String, Object> velocityContext = MacroUtils.defaultVelocityContext();
    	velocityContext.put("table", list);
    	velocityContext.put(PARAM_DISPLAY_TOTAL_FOR_COLUMNS, displayTotalForColumns);
    	result.append(VelocityUtils.getRenderedTemplate("templates/pivot-table.vm", velocityContext));
    	 */
    	
    	result.append(createHTMLTable(list));
    	
    	if (PARAM_DISPLAY_BODY_TABLES_AFTER.equals(dataDisplay)) {
    		result.append(elements);
    	}
    	return result.toString();
    }
    
    private void updateUpperLeftLabel(List<List<String>> list, String columnNameForRows, String colunmNameForColumns, String calculationForRowsLabel) {
    	if (columnNameForRows == null && colunmNameForColumns == null) {
    		for (List<String> row : list) {
    			row.remove(0);
    		}
    	}
		if (colunmNameForColumns != null && columnNameForRows != null) {
			// Replace upper left label (which contains the name of the row or column) with the value of calculationForRowsLabel
			List<String> firstRow = list.get(0);
	    	firstRow.remove(0);
			firstRow.add(0, calculationForRowsLabel);
		}
	}

	@Override
    public BodyType getBodyType() {
    	return BodyType.RICH_TEXT;
    }
    
    @Override
    public OutputType getOutputType() {
    	return OutputType.BLOCK;
    }
	
	List<List<String>> buildPivotTable(Map<String, Map<String, List<String>>> rowsAndColumnsMap, Map<String, List<String>> rowsMap, String columnName, String typeOfCalculation, ColumnValueParameters regexpForValues, ConversionContext context) {
		List<List<String>> table = new ArrayList<List<String>>();
		List<String> row = new ArrayList<String>();
		table.add(row);
		row.add(columnName);
		for (Entry<String, Map<String, List<String>>> e : rowsAndColumnsMap.entrySet()) {
			row.add(e.getKey());
		}
		
		for (String s : rowsMap.keySet()) {
			row = new ArrayList<String>();
			table.add(row);
			row.add(s);
			for (Entry<String, Map<String, List<String>>> e : rowsAndColumnsMap.entrySet()) {
				List<String> values = e.getValue().get(s);
				row.add(Utils.calculatePivotValue(values, typeOfCalculation, regexpForValues, context, bandanaManager));
			}
		}
		return table;
	}
	
	private void addGrandTotals(List<List<String>> table, Map<String, List<String>> rowsMap, Map<String, List<String>> columnsMap, String totalForColumnsLabel, String totalForRowsLabel, String typeOfCalculation, ColumnValueParameters regexpForValues, ConversionContext context) {
		if (totalForRowsLabel != null) {
			// Add the Grand Total header
			table.get(0).add(totalForRowsLabel);
			// Calculate grand total for each row
			for (int i = 1; i < table.size(); i++) {
				List<String> e = table.get(i); 
				String rowKey = e.get(0); 
				log.debug("addGrandTotals: rowKey = "+rowKey);
				log.debug("addGrandTotals: rowsMap.get(rowKey) = "+rowsMap.get(rowKey));
				String result = Utils.calculatePivotValue(rowsMap.get(rowKey), typeOfCalculation, regexpForValues, context, bandanaManager);
				log.debug("addGrandTotals: result = "+result);
				e.add(result);
			}
		}
		if (totalForColumnsLabel != null) {
			List<String> rowForTotals = new ArrayList<String>();
			// Add the Grand Total header
			rowForTotals.add(totalForColumnsLabel);
			// Calculate grand total for each column
			List<String> e = table.get(0);
			for (int j = 1; j < e.size(); j++) {
				String columnKey = e.get(j);
				log.debug("addGrandTotals: columnKey = "+columnKey);
				log.debug("addGrandTotals: columnKey.get(columnKey) = "+columnsMap.get(columnKey));
				String result = "";
				log.debug("addGrandTotals: j = "+j+ " e.size()-1 = "+(e.size()-1));
				if ((totalForRowsLabel != null && j == e.size()-1) || e.size() == 2) {
					// The last element is the Grand Total
					// Group all column map value in the same List
					List<String> allStrings = new ArrayList<String>();
					for (List<String> l : rowsMap.values()) {
						allStrings.addAll(l);
					}
					result = Utils.calculatePivotValue(allStrings, typeOfCalculation, regexpForValues, context, bandanaManager);
				} else {
					result = Utils.calculatePivotValue(columnsMap.get(columnKey), typeOfCalculation, regexpForValues, context, bandanaManager);
				}
				log.debug("addGrandTotals: result = "+result);
				rowForTotals.add(result);
			}
			table.add(rowForTotals);			
		}
	}
	
	/*
	 * Create an HTML table from a 2 dimension List of String. Elements of the first list will correspond to a row of the table. Element of the second list will correspond to the columns of the table
	 * @param rows the 2 dimension table
	 * @return a table Element representing the 2 dimensional table with a header
	 */
	private static Element createHTMLTable(List <List<String>> rows) {
		Element table = new Element(Tag.valueOf("table"), "").addClass("confluenceTable").attr("id", "pivot");
		Element theadOrtbody = table.appendElement("thead");
		String tdOrTh;
		for (int i = 0; i < rows.size(); i++) {
			List<String> row = rows.get(i);
			if (i == 1) {
				theadOrtbody = table.appendElement("tbody");
			}
			Element tr = theadOrtbody.appendElement("tr").addClass("confluenceTr");
			for (int j = 0; j < row.size(); j++) {
				//Put a header on the first column if there is only one row
				//or on the first column and the first row otherwise				
				if ((rows.size() == 1 && i == 0) || (rows.size() > 1 && (i == 0 || j == 0))) {
					tdOrTh = "th";
				} else {
					tdOrTh = "td";
				}
				Element td = tr.appendElement(tdOrTh).addClass("confluenceT"+tdOrTh.substring(1, 2));
				td.append(row.get(j));
			}
		}
		return table;
	}
}