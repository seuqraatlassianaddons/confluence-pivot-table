package com.seuqra.confluence.pivottable;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Tag;
import org.jsoup.select.Elements;

import com.atlassian.bandana.BandanaManager;
import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.renderer.UserMacroConfig;
import com.atlassian.confluence.setup.bandana.ConfluenceBandanaContext;
import com.udojava.evalex.Expression;

public class Utils {
	public static final String BACKGROUND_STYLE = "border: 2px solid #bbb; padding: 4px 12px; border-radius: 5px;-moz-border-radius:5px; -webkit-border-radius: 5px; display: inline-block; text-align:center; min-width:60px;";
	private static Log log = LogFactory.getLog("com.seuqra.confluence.pivottable");
	
	static String getTdText(int position, Elements tds) {
		return getTdText(position, tds, null);
	}
		
	static String getTdText(int position, Elements tds, String valueIfEmpty) {
		try {
			Element td = tds.get(position);
			String text = td.text().replaceAll("\u00a0"," ").trim();
			if ((text == null || text.length() == 0) && (valueIfEmpty != null)) {
				text = valueIfEmpty;
			}
			return text;
		} catch (ArrayIndexOutOfBoundsException e) {
			return null;
		} catch (NullPointerException e) {
			return null;
		}
	}
	
	public static void addColorInSpanTag(Element td, String color) {
		Element span = new Element(Tag.valueOf("span"), "");
		span.attr("style", "background-color: "+color+"; " +BACKGROUND_STYLE);
		span = span.text(td.text());
		td.text("");
		td.appendChild(span);
	}
	
	public static int columnPosition(String name, Element table) {
		int pos = -1;
		boolean found = false;

		String columnName = name;
		if (columnName == null || table == null) {
			return -1;
		}
		columnName = columnName.trim().toLowerCase();
		try {
			Iterator<Element> iterator = table.select("th").iterator();
			while (iterator.hasNext()) {
				Element tmp = iterator.next();
				if (!tmp.hasAttr("colspan") || "1".equals(tmp.attr("colspan"))) {
					pos++;
				} 
				if (columnName.equals(tmp.text().trim().toLowerCase())) {
					found = true;
					break;
				}
			}
		} catch (Exception e) {

		}

		return found ? pos:-1;
	}
	
	static void addValueInMapOfMap(ColumnValueParameters rowColumnParameters, ColumnValueParameters columnColumnParameters, String value, Map<String, Map<String, List<String>>> rowsAndColumnsMap) {
		if (rowColumnParameters != null && columnColumnParameters != null) {
			String labelValueForRow = rowColumnParameters.getReplacement();
			String labelValueforColumn = columnColumnParameters.getReplacement();
			if (labelValueForRow != null && labelValueforColumn != null) {
				Map<String, List <String>> map = rowsAndColumnsMap.get(labelValueforColumn);
				if (map == null) {
					map = new TreeMap<String, List<String>>();
					rowsAndColumnsMap.put(labelValueforColumn, map);
				}
				addValueInMap(value, map, labelValueForRow);				
			}
		}
	}
	
	static void addValueInMap(ColumnValueParameters columnParameters, Element td, String value, Map<String, List<String>> map) {
		if (columnParameters != null) {
			String labelValue = columnParameters.getReplacement();
			if (labelValue != null) {
				String color = columnParameters.getColor();
				if (color != null) {
					Utils.addColorInSpanTag(td, color);
				}
				addValueInMap(value, map, labelValue);
			}
		}
	}
	
	private static void addValueInMap(String value, Map<String, List<String>> map, String key) {
		List<String> listOfValues = map.get(key);
		
		if (listOfValues == null) {
			listOfValues = new ArrayList<String>();
			map.put(key, listOfValues);
		}
		listOfValues.add(value); 
	}
	
	public static String calculatePivotValue(List <String> values, String typeOfCalculation, ColumnValueParameters regexpForValues, ConversionContext context, BandanaManager bandanaManager) {
		if (values != null) {
			if (PivotTable.PARAM_TYPE_OF_CALCULATION_VALUE_COUNT.equals(typeOfCalculation)) {
				return  Integer.toString(values.size());
			} else if (PivotTable.PARAM_TYPE_OF_CALCULATION_VALUE_SUM.equals(typeOfCalculation)) {
				List<String> updatedValues = applyRegExp(values, regexpForValues);
				Float sum = sumValues(updatedValues);
				if (sum == sum.intValue()) {
					return ""+sum.intValue();
				}
				return sum.toString();
			} else if (PivotTable.PARAM_TYPE_OF_CALCULATION_VALUE_AVERAGE.equals(typeOfCalculation)) {
				List<String> updatedValues = applyRegExp(values, regexpForValues);
				Float div = averageValues(updatedValues);
				if (div == div.intValue()) {
					return ""+div.intValue();
				}
				return Float.toString(div);
			} else {
				// Delegate the rendering to the pivot-values-rendering user macro
				return renderPivotValuesRenderingMacro(values,
						typeOfCalculation, context, bandanaManager);
			}
		}
		return "";
	}

	/*
	 * Delegates the rendering of the pivot value cell to the pivot-values-rendering macro
	 * @param values the values to compute and to render
	 * @param typeOfRendering the type of rendering the user macro should implement
	 * @param context the conversion context
	 * @param bandanaManager
	 * @return the rendered value
	 */
	private static String renderPivotValuesRenderingMacro(List<String> values,
			String typeOfRendering, ConversionContext context,
			BandanaManager bandanaManager) {
		log.debug("User Macro " + typeOfRendering);
		Map<String, String> parameters = new HashMap<String, String>();
		parameters.put(PivotTable.PIVOT_TABLE_USER_MACRO_PARAM_TYPE_OF_RENDERING, typeOfRendering);
		// Set the values as a property of the Conversion Context
		context.setProperty(PivotTable.PIVOT_TABLE_USER_MACRO_PARAM_BODY_TABLES_VALUES, values);
		return renderUserMacro(PivotTable.PIVOT_TABLE_USER_MACRO_PIVOT_VALUES_RENDERING, parameters, context, bandanaManager);
	}

	/*
	 * Apply a regexp and a replacement to the list
	 * @param the values to replace
	 * @param regexpForValues The regexp to apply to each value
	 * @return the modified list
	 */
	private static List<String> applyRegExp(List<String> values, ColumnValueParameters regexpForValues) {
		if (regexpForValues == null || regexpForValues.getRegexp() == null || regexpForValues.getReplacement() == null) {
			return values;
		}
		// Apply regexp
		List<String> updatedValues = new ArrayList<String>(values.size());
		for (String value : values) {
			// Default value
			String updatedValue = value;
			try {
				updatedValue = regexpForValues.getReplacement(value);
				log.debug("Replacement value after regexp computing = " + updatedValue);
			} catch (Exception e) {
				log.error("Error during regexp replacement for string "+value, e);
			}
			updatedValues.add(updatedValue);
		}
		return updatedValues;
	}
	
	/*
	 * Sum the values provided by the list
	 * @param values The values to sum
	 * @return The sum
	 */
	private static Float sumValues(List<String> values) {
		BigDecimal sumBigDecimal = new BigDecimal(0);
		for (String value : values) {
			try {
				Expression expression = new Expression(value);
				//expression.setPrecision(2);
				BigDecimal expressionValue = expression.eval();
				log.debug("Result after formula computation = " + expressionValue);
				sumBigDecimal = sumBigDecimal.add(expressionValue);
			} catch (Exception e) {
				log.error(" Cannot transform "+value+" to a BigDecimal", e);
			}
		}
		log.debug("BigDecimal sum = " + sumBigDecimal);
		
		Float sum = sumBigDecimal.floatValue();
		return sum;
	}
	
	/*
	 * Average the values provided by the list
	 * @param values The values to average
	 * @return The average
	 */
	private static Float averageValues(List<String> values) {
		Float sum = sumValues(values);
		return sum/values.size();
	}
	
	/*
	 * Convert a list of String elements to a string where each element of the list is separated by a comma. 
	 * If an element contains a comma, a backslash character is added before
	 * @param list The list to serialize
	 * @return the serialized List of String
	 */
	private static String serializeList(List<String> list) {
		/*
		for (String element : list) {
			element.replaceAll(",","\\,");
		}*/
		
	    Iterator<String> i = list.iterator();
		if (! i.hasNext())
		    return "";

		StringBuilder sb = new StringBuilder();
		for (;;) {
		    String e = i.next().replaceAll(",","\\\\,");
		    sb.append(e);
		    if (! i.hasNext()) {
		    	return sb.toString();
		    }
		    sb.append(", ");
		}
	}
	
	/*
	 * Convert a String separated by a comma to an array of String
	 * If an element contains a comma, it should have been escaped by a backslash during the serialization
	 * @param serializedList the serialized List of String
	 * @return the deserialized Strings
	 */
	private static String[] deserializeList(String serializedList) {
		String regex = "(?<!\\\\),";
		String[] deserializedList = serializedList.split(regex);
		for (int i = 0; i < deserializedList.length; i++) {
			deserializedList[i] = deserializedList[i].replaceAll("\\\\,",",");
		}
		return deserializedList;
	}
	
	private static Map<String, UserMacroConfig> getUserMacros(BandanaManager bandanaManager) {
		Map result = (Map) bandanaManager.getValue(new ConfluenceBandanaContext(), "atlassian.confluence.user.macros");
		return result;
	}
	
	static UserMacroConfig getUserMacroConfig(BandanaManager bandanaManager, String macroName) {
		try {
			return getUserMacros(bandanaManager).get(macroName);
		} catch (Exception e) {
			log.error("Cannot retrieve macro "+ macroName, e);
			return null;
		}
	}
	
	private static String renderUserMacro(String macroName, Map<String, String> parameters, ConversionContext context, BandanaManager bandanaManager/*, MacroManager macroManager*/) {
		String userMacroRendering = "";
		try {
			UserMacroConfig userMacroConfig = getUserMacroConfig(bandanaManager, macroName.trim());
			if (userMacroConfig != null) {
				Macro userMacro = userMacroConfig.toMacro();
				if (userMacro != null) {
					userMacroRendering = userMacro.execute(parameters, "", context);
					log.debug("userMacroRendering of macro "+ macroName + " = " + userMacroRendering);
				}
			} else {
				log.error("Cannot retrieve user macro "+ macroName);
			}
		} catch (Exception e) {
			log.error("Cannot execute user macro "+ macroName, e);
		}
		return userMacroRendering;
	}
}