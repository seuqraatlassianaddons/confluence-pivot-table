package com.seuqra.confluence.pivottable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.atlassian.confluence.macro.browser.beans.MacroParameter;
import com.atlassian.confluence.macro.browser.beans.MacroParameterType;
import com.atlassian.confluence.renderer.UserMacroConfig;
import com.atlassian.confluence.util.i18n.Message;
import com.atlassian.sal.api.message.I18nResolver;

@XmlRootElement(name="userMacroMetaData")
public class UserMacroConfigResourceModel implements Comparable<Object> {
	
	@XmlElement
	private String name;
	@XmlElement
	private String title;
	//@XmlElement
	private String iconLocation;
	//@XmlElement
	private String description;
	//@XmlElement
	private String template;
	//@XmlElement
	private boolean hasBody;
	//@XmlElement
	private String bodyType;
	//@XmlElement
	private String documentationUrl;
	//@XmlElement
	private Set<String> categories;
	//@XmlElement
	private boolean hidden;
	@XmlElement
	private List<MacroParameterResourceModel> parameters;
	
	private I18nResolver i18n;

	public UserMacroConfigResourceModel(UserMacroConfig m, I18nResolver i18n) {
		this(m.getName(), m.getTitle(), m.getIconLocation(), m.getDescription(), m.getTemplate(),
				m.isHasBody(), m.getBodyType(), m.getDocumentationUrl(), m.getCategories(), m.isHidden(), m.getParameters(), i18n);
	}

	private UserMacroConfigResourceModel(String name, String title, String iconLocation, String description, String template,
			boolean hasBody, String bodyType, String documentationUrl, Set<String> categories, boolean hidden, 
			List<MacroParameter> parameters, I18nResolver i18n) {
		this.name = name;
		this.title = title;
		this.iconLocation = iconLocation;
		this.description = description;
		this.template = template;
		this.hasBody = hasBody;
		this.bodyType = bodyType;
		this.documentationUrl = documentationUrl;
		this.categories = categories;
		this.hidden = hidden;
		this.parameters = createMacroParameterResourceModel(parameters);
		this.i18n = i18n;
	}
	
	private List<MacroParameterResourceModel> createMacroParameterResourceModel(List<MacroParameter> parameters) {
		List<MacroParameterResourceModel> macroParameterResourcesModel = new ArrayList<MacroParameterResourceModel>();
		for (MacroParameter macroParameter : parameters) {
			macroParameterResourcesModel.add(new MacroParameterResourceModel(macroParameter));
		}
		return macroParameterResourcesModel;
	}

	public String getName() {
		return this.name;
	}

	public String getTemplate() {
		return this.template;
	}

	public boolean isHasBody() {
		return this.hasBody;
	}

	public String getBodyType() {
		return this.bodyType;
	}

	public String getTitle() {
		return this.title;
	}

	public String getDescription() {
		return this.description;
	}

	public Set<String> getCategories() {
		if (this.categories == null) {
			return Collections.emptySet();
		}
		return Collections.unmodifiableSet(this.categories);
	}

	public String getIconLocation() {
		return this.iconLocation;
	}

	public String getDocumentationUrl() {
		return this.documentationUrl;
	}

	public boolean isHidden() {
		return this.hidden;
	}

	public List<MacroParameterResourceModel> getParameters() {
		return this.parameters;
	}

	@Override
	public int compareTo(Object arg0) {
		if (arg0 instanceof UserMacroConfigResourceModel) {
			UserMacroConfigResourceModel macroMetaDataResourceModel = (UserMacroConfigResourceModel) arg0;
			return getTitle().compareToIgnoreCase(macroMetaDataResourceModel.getTitle());
		} else {
			return 1;
		}
	}
	
	public class MacroParameterResourceModel  {
		@XmlElement
		private String name;
		@XmlElement
		private MacroParameterType type;
		@XmlElement
		private String defaultValue;
		//@XmlElement
		private boolean required;
		//@XmlElement
		private boolean multiple;
		//@XmlElement
		private Set<String> aliases;
		@XmlElement
		private List<String> enumValues;
		//@XmlElement
		private Properties options;
		//@XmlElement
		private boolean hidden;
		private Message displayName;
		private Message description;

		public MacroParameterResourceModel(MacroParameter mp) {
			this(mp.getName(), mp.getDisplayName(), mp.getDescription(), mp.getType(), mp.isRequired(), mp.isMultiple(), mp.getDefaultValue(), mp.isHidden() ,mp.getEnumValues(), mp.getOptions(), mp.getAliases());
		}
		
		private MacroParameterResourceModel(String name, Message displayName, Message description,
				MacroParameterType type, boolean required, boolean multiple,
				String defaultValue, boolean hidden, List<String> enumValues, Properties options, Set<String> aliases) {
			this.name = name;
			this.displayName = displayName;
			this.type = type;
			this.required = required;
			this.multiple = multiple;
			this.hidden = hidden;
			this.aliases = aliases;
			this.enumValues = enumValues;
			this.options = options;

			if (type == MacroParameterType.BOOLEAN)
				this.defaultValue = Boolean.valueOf(defaultValue).toString();
			else
				this.defaultValue = defaultValue;
		}

		public boolean isHidden() {
			return this.hidden;
		}

		public String getName() {
			return this.name;
		}

		public MacroParameterType getType() {
			return this.type;
		}

		public String getDefaultValue() {
			return this.defaultValue;
		}

		public boolean isRequired() {
			return this.required;
		}

		public boolean isMultiple() {
			return this.multiple;
		}

		public Set<String> getAliases() {
			return this.aliases;
		}

		public List<String> getEnumValues() {
			if (this.type == MacroParameterType.ENUM) {
				return this.enumValues;
			}
			return null;
		}

		public Properties getOptions() {
			return this.options;
		}

		public Message getDisplayName() {
			return displayName;
		}

		public Message getDescription() {
			return description;
		}

		public String toString() {
			return new StringBuilder().append(this.name)
					.append((this.required) ? " : required " : " : ")
					.append(this.type).toString();
		}
	}
}