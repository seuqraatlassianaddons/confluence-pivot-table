package com.seuqra.confluence.pivottable;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TreeMap;

import com.atlassian.sal.api.message.I18nResolver;

public class ColumnParameters {
	
	ColumnValueParameters[] columnValuesParameters;
	
	private static Comparator<String> CUSTOM_COMPARATOR = new SpecialCharactersComparator<String>();
	
	public ColumnValueParameters[] getColumnValuesParameters() {
		return columnValuesParameters;
	}

	public ColumnParameters(String param, I18nResolver i18n) {
		if (param != null) {
			StringTokenizer st = new StringTokenizer(param, ",");
			int size = st.countTokens();
			columnValuesParameters = new ColumnValueParameters[size];
			for (int i = 0; i < size; i++) {
				columnValuesParameters[i] = new ColumnValueParameters(st.nextToken(), i18n);
			}
		}
	}
	
	public ColumnValueParameters getColumnParametersForPatternMatching(String s) {
		if (s == null) {
			return null;
		}
		if (columnValuesParameters == null) {
			// mode autoPopulate
			return new ColumnValueParameters(s);
		}
		for (int i = 0; i < columnValuesParameters.length; i++) {
			if (columnValuesParameters[i].matches(s)) {
				return columnValuesParameters[i];
			}
		}
		return null;
	}
	
	public Map<String, List<String>> initMapWithEmptyValues() {
		Map<String, List<String>> map = new TreeMap<String, List<String>>(CUSTOM_COMPARATOR);

		if (columnValuesParameters == null) {
			// mode autoPopulate
			return map;
		}
		List<String> list = new ArrayList<String>();
		for (int i = 0; i < columnValuesParameters.length; i++) {
			map.put(columnValuesParameters[i].getReplacement(), list);
		}
		return map;
	}
	
	Map<String, Map<String, List<String>>> initPivotMapWithEmptyValues() {
		Map<String, Map<String, List <String>>> groupMap = new TreeMap<String, Map<String, List<String>>>(CUSTOM_COMPARATOR);

		if (columnValuesParameters == null) {
			//mode autoPopulate
			return groupMap;
		}
		for (int i = 0; i < columnValuesParameters.length; i++) {
			Map<String, List <String>> map = new TreeMap<String, List <String>>(CUSTOM_COMPARATOR);
			groupMap.put(columnValuesParameters[i].getReplacement(), map);
		}
		return groupMap;
	}
	
	private static class SpecialCharactersComparator<S> implements Comparator<String> {
		// Copy - paste from http://stackoverflow.com/questions/23362143/change-sort-order-of-strings-includes-with-a-special-character-e-g

	    // declare in order of desired sort
	    private final String[] specialChars = { "(", "_", "@", "#", "$", "%", "&" };

	    @Override
	    public int compare(String o1, String o2) {
	        /*
	         * CASES
	         * 
	         * 1. Both start with same special char
	         * 
	         * 2. Both start with a special char
	         * 
	         * 3. One starts with a special char
	         * 
	         * 4. None starts with a special char
	         */

	        int o1SpecialIndex = -1;
	        int o2SpecialIndex = -1;

	        for (int i = 0; i < specialChars.length; i++) {
	            if (o1.startsWith(specialChars[i])) {
	                o1SpecialIndex = i;
	            }
	            if (o2.startsWith(specialChars[i])) {
	                o2SpecialIndex = i;
	            }
	        }

	        // case 1:
	        if (o1SpecialIndex != -1 && o1SpecialIndex == o2SpecialIndex) {
	            return compare(o1.substring(1), o2.substring(1));
	        }

	        // case 2:
	        if (o1SpecialIndex != -1 && o2SpecialIndex != -1) {
	            return o2SpecialIndex - o1SpecialIndex;
	        }

	        // case 3:
	        if (o1SpecialIndex != -1) {
	            return 1;
	        }
	        if (o2SpecialIndex != -1) {
	            return -1;
	        }

	        // case 4:
	        return o1.compareTo(o2);
	    }
	}
}
