(function($) {

    var columnHeaders = [];
    var PivotTableMacroJsOverride = function() {
    };

    PivotTableMacroJsOverride.prototype.getMacroDetailsFromSelectedMacro = function(metadataList, macro) {
        // The body table has to be inserted in the DOM to be retrieved by jQuery when calling tableToJSON().
        $("body").append("<table id='inputTable'>");
        $("#inputTable").html(macro.body);
        
        var tables = $("#inputTable").tableToJSON();
        if (tables[0]) {
        	columnHeaders = Object.keys(tables[0]);
    	}
        columnHeaders.sort();
        //TODO: Remove table inputTable
    };
    
    PivotTableMacroJsOverride.prototype.beforeParamsSet = function(params) {
        fillEnumsForParameter(params, "columnNameForValues", columnHeaders, false, true);
        fillEnumsForParameter(params, "columnNameForRows", columnHeaders, false, true);
        fillEnumsForParameter(params, "columnNameForColumns", columnHeaders, false, true);
        fetchAndDisplayCalculationTypes(params);
        return params;
    };
    
    function fetchAndDisplayCalculationTypes(params) {
    	// Clone params
    	var pivotParams = JSON.parse(JSON.stringify(params));
        $.ajax({
            type: 'GET',
            url: AJS.contextPath() + '/rest/pivot/1.0/usermacros/pivot-values-rendering',
            contentType:'application/json',
            // Synchronous call because the enum list has to be updated before returning params in method beforeParamsSet
            async: false
        }).done(function(userMacro) {
        	var parameters = userMacro.parameters;
        	for (var i = 0; i < parameters.length; ++i) {
        		var param = parameters[i];
        		if (param.name === 'typeOfRendering') {
        			var typesOfCalculation = param.enumValues;
        	        fillEnumsForParameter(pivotParams, "typeOfCalculation", typesOfCalculation, true, false);
        			break;
        		}
        	}
        }).fail(function() {

        });
    };
    
    function fillEnumsForParameter(params, parameterString, values, required, keepCurrentValue) {
        var currentValue, parameter, parameterName;
        var arrayLength = values.length;

        if (params[parameterString]) {
        	currentValue = $.trim(params[parameterString]);
        } else if (required) {
        	if (values.length > 0) {
        		// If there is no current value, set it to the first element of the column headers
        		currentValue = values[0];
        	} else {
        		// Need a default value to be able to save the macro the first time without body
        		currentValue = "-";
        	}
        }

        // All the parameters defined in atlassian-plugin.xml are prefixed by "macro-param-" string in the DOM
        parameterName ='macro-param-'+parameterString;
        parameter = $('#'+parameterName);

        // Workaround for issue https://bitbucket.org/seuqra/confluence-add-ons/issues/1/i-cannot-load-proper-jira-table-into-pivot
        // If the headers can not be computed, do no create an enum list
        if (values.length === 0) {
        	if (!$(parameter).is('select')) {
        		// the input field is not a select
	        	if (currentValue) {
	        		parameter.attr('value',currentValue);
	        	}
        	} else {
        		// TODO: Manage the case where currentValue is empty
	        	parameter.replaceWith('<input type="text" class="text macro-param-input" id="'+parameterName+'" value="'+currentValue+'">');	        		
	        }
            return;
        } else {
        	// Header found in the body. Replace the input field by an enum
        	if (!$(parameter).is('select')) {
        		// the input field is not a select
        		parameter.replaceWith('<select class="select macro-param-input" id="'+parameterName+'">');
        		parameter = $('#'+parameterName);
        	}
        }

        // See https://bitbucket.org/seuqra/confluence-pivot-table/issues/6/no-empty-options-in-values-columns-and
        // Add an empty value in top of the list if this parameter is optional
        if (!required) {
            parameter.append($('<option></option>').text(''));
        }
        
        var currentValueFoundInValues = false;
        var toSelect = false;

        for (var i = 0; i < arrayLength; i++) {
        	toSelect = false;
        	// Select the first element if there is no current value
        	// otherwise the element corresponding to the currentValue if exists
        	if (currentValue == values[i]) {
        		toSelect = true;
        		currentValueFoundInValues = true;
        	}
            var selected = toSelect ? 'selected="selected"' : '';
            parameter.append($('<option value="' +values[i]+'" ' + selected + '></option>').text(values[i]));
        }
        
        // The current value is not in the columnHeaders list
        // Add it in the list
        if (keepCurrentValue && currentValue && !currentValueFoundInValues) {
            parameter.append($('<option selected="selected"></option>').text(currentValue));
        }
    }

    AJS.MacroBrowser.setMacroJsOverride("pivot-table", new PivotTableMacroJsOverride());

})(AJS.$);