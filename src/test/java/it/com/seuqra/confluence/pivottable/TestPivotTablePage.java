package it.com.seuqra.confluence.pivottable;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntil;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.nullValue;

import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.atlassian.confluence.webdriver.pageobjects.ConfluenceTestedProduct;
import com.atlassian.confluence.webdriver.pageobjects.page.ConfluenceLoginPage;
import com.atlassian.pageobjects.TestedProductFactory;

public class TestPivotTablePage {
	private static ConfluenceTestedProduct confluenceTestedProduct = TestedProductFactory.create(ConfluenceTestedProduct.class);
	
	private void testIfPivotTableExists(PivotTablePage pivotPage, boolean shouldExist) {
		if (shouldExist) {
			waitUntil(pivotPage.getPivotTableId(), (Matcher<String>) equalTo(PivotTablePage.PIVOT_TABLE_ID));
		} else {
			waitUntil(pivotPage.getPivotTableId(), nullValue());
		}
	}
	
	protected PivotTablePage navigateToAndBind(String page) {
		PivotTablePage.PAGE = page;
		PivotTablePage pivotTablePage = (PivotTablePage) confluenceTestedProduct.getPageBinder().navigateToAndBind(PivotTablePage.class);
		return pivotTablePage;
	}
	
	private PivotTablePage testErrorMessage(String page, String expectedMessage) {
		PivotTablePage pivotTablePage = navigateToAndBind(page);
		testIfPivotTableExists(pivotTablePage, false);
		waitUntil(pivotTablePage.geMainContentText(), (Matcher<String>) equalTo(expectedMessage));
		return pivotTablePage;
	}
	
	
	private PivotTablePage testPivotTableCellTextEquals(String page, String expectedMessage, int rowPosition, int columnPosition) {
		PivotTablePage pivotTablePage = navigateToAndBind(page);
		return testPivotTableCellTextEquals(pivotTablePage, expectedMessage, rowPosition, columnPosition);
	}
	
	private PivotTablePage testPivotTableCellTextEquals(PivotTablePage pivotTablePage, String expectedMessage, int rowPosition, int columnPosition) {
		testIfPivotTableExists(pivotTablePage, true);
		waitUntil(pivotTablePage.getPivotTableCellText(rowPosition, columnPosition), (Matcher<String>) equalTo(expectedMessage));
		return pivotTablePage;
	}
	
	private PivotTablePage testPivotTableCellTextContains(PivotTablePage pivotTablePage, String expectedMessage, int rowPosition, int columnPosition) {
		waitUntil(pivotTablePage.getPivotTableCellText(rowPosition, columnPosition), (Matcher<String>) Matchers.containsString(expectedMessage));
		return pivotTablePage;
	}
	
	private PivotTablePage testPivotTableCellTextStartsWith(PivotTablePage pivotTablePage, String expectedMessage, int rowPosition, int columnPosition) {
		waitUntil(pivotTablePage.getPivotTableCellText(rowPosition, columnPosition), (Matcher<String>) Matchers.startsWith(expectedMessage));
		return pivotTablePage;
	}
	
	private PivotTablePage testPivotTableCellTextEndsWith(PivotTablePage pivotTablePage, String expectedMessage, int rowPosition, int columnPosition) {
		waitUntil(pivotTablePage.getPivotTableCellText(rowPosition, columnPosition), (Matcher<String>) Matchers.endsWith(expectedMessage));
		return pivotTablePage;
	}
	
	@BeforeClass
	public static void login() {
		ConfluenceLoginPage confluenceLoginPage = confluenceTestedProduct.gotoLoginPage();
		confluenceLoginPage.login("admin", "admin", true);
	}
	
	@AfterClass
	public static void logout() {
		confluenceTestedProduct.logOut();
	}
	
	//Tests Pivot - Values
	@Test
	public void testPivotTableRenderingWithDefaultParameters() {
		testPivotTableCellTextEquals("Pivot+-+Count+of+first+column+without+total", "Count of Date of Sale", 1, 1);
	}
		
	@Test
	public void testPivotTableRenderingWithCountOfPerUnitPriceValues() {
		testPivotTableCellTextEquals("Pivot+-+Count+of+Per+Unit+Price+without+totals", "Count of Per Unit Price", 1, 1);		
	}
		
	@Test
	public void testPivotTableRenderingWithSumOfPerUnitPriceValues() {
		testPivotTableCellTextEquals("Pivot+-+Sum+of+Per+Unit+Price+without+totals", "Sum of Per Unit Price", 1, 1);		
	}
		
	@Test
	public void testPivotTableRenderingWithAverageOfPerUnitPriceValues() {
		testPivotTableCellTextEquals("Pivot+-+Average+of+Per+Unit+Price+without+totals", "Average of Per Unit Price", 1, 1);		
	}
		
	// Pivot - Values - Rows
	@Test
	public void testPivotTableRenderingWithCountOfPerUnitPriceValuesPerSalesPersonAsRow() {
		String page = "Pivot+-+Count+of+Per+Unit+Price+per+Sales+Person";
		testPivotTableRenderingWithCountOfPerUnitPriceValuesPerSalesPersonAsRow(page, false);
	}		
	
	@Test
	public void testPivotTableRenderingWithCountOfPerUnitPriceValuesPerSalesPersonAsRowWithTotalsForColumns() {
		String page = "Pivot+-+Count+of+Per+Unit+Price+per+Sales+Person+with+totals+for+columns";
		testPivotTableRenderingWithCountOfPerUnitPriceValuesPerSalesPersonAsRow(page, true);
	}		
	
	private void testPivotTableRenderingWithCountOfPerUnitPriceValuesPerSalesPersonAsRow(String page, boolean totalsForColumns) {
		PivotTablePage pivotTablePage = testPivotTableCellTextEquals(page, "Sales Person", 1, 1);		
		testPivotTableCellTextEquals(pivotTablePage, "Count of Per Unit Price", 1, 2);
		testPivotTableCellTextEquals(pivotTablePage, "Imran", 2, 1);
		testPivotTableCellTextEquals(pivotTablePage, "4", 2, 2);
		testPivotTableCellTextEquals(pivotTablePage, "Larry", 3, 1);
		testPivotTableCellTextEquals(pivotTablePage, "3", 3, 2);
		if (totalsForColumns) {
			testPivotTableCellTextEquals(pivotTablePage, "Grand Total", 4, 1);
			testPivotTableCellTextEquals(pivotTablePage, "7", 4, 2);
		}
	}		

	@Test
	public void testPivotTableRenderingWithSumOfPerUnitPriceValuesPerSalesPersonAsRow() {
		String page = "Pivot+-+Sum+of+Per+Unit+Price+per+Sales+Person";
		testPivotTableRenderingWithSumOfPerUnitPriceValuesPerSalesPersonAsRow(page, false);
	}		
	
	@Test
	public void testPivotTableRenderingWithSumOfPerUnitPriceValuesPerSalesPersonAsRowWithTotalsForColumns() {
		String page = "Pivot+-+Sum+of+Per+Unit+Price+per+Sales+Person+with+totals+for+columns";
		testPivotTableRenderingWithSumOfPerUnitPriceValuesPerSalesPersonAsRow(page, true);
	}		
	
	private void testPivotTableRenderingWithSumOfPerUnitPriceValuesPerSalesPersonAsRow(String page, boolean totalsForColumns) {
		PivotTablePage pivotTablePage = testPivotTableCellTextEquals(page, "Sales Person", 1, 1);		
		testPivotTableCellTextEquals(pivotTablePage, "Sum of Per Unit Price", 1, 2);
		testPivotTableCellTextEquals(pivotTablePage, "Imran", 2, 1);
		testPivotTableCellTextEquals(pivotTablePage, "49858", 2, 2);
		testPivotTableCellTextEquals(pivotTablePage, "Larry", 3, 1);
		testPivotTableCellTextEquals(pivotTablePage, "62008", 3, 2);
		if (totalsForColumns) {
			testPivotTableCellTextEquals(pivotTablePage, "Grand Total", 4, 1);
			testPivotTableCellTextEquals(pivotTablePage, "111866", 4, 2);
		}
	}		
	
	@Test
	public void testPivotTableRenderingWithAverageOfPerUnitPriceValuesPerSalesPersonAsRow() {
		String page = "Pivot+-+Average+of+Per+Unit+Price+per+Sales+Person";
		testPivotTableRenderingWithAverageOfPerUnitPriceValuesPerSalesPersonAsRow(page, false);
	}		
	
	@Test
	public void testPivotTableRenderingWithAverageOfPerUnitPriceValuesPerSalesPersonAsRowWithTotalsForColumns() {
		String page = "Pivot+-+Average+of+Per+Unit+Price+per+Sales+Person+with+totals+for+columns";
		testPivotTableRenderingWithAverageOfPerUnitPriceValuesPerSalesPersonAsRow(page, true);
	}		
	
	private void testPivotTableRenderingWithAverageOfPerUnitPriceValuesPerSalesPersonAsRow(String page, boolean totalsForColumns) {
		PivotTablePage pivotTablePage = testPivotTableCellTextEquals(page, "Sales Person", 1, 1);		
		testPivotTableCellTextEquals(pivotTablePage, "Average of Per Unit Price", 1, 2);
		testPivotTableCellTextEquals(pivotTablePage, "Imran", 2, 1);
		testPivotTableCellTextEquals(pivotTablePage, "12464.5", 2, 2);
		testPivotTableCellTextEquals(pivotTablePage, "Larry", 3, 1);
		testPivotTableCellTextEquals(pivotTablePage, "20669.334", 3, 2);
		if (totalsForColumns) {
			testPivotTableCellTextEquals(pivotTablePage, "Grand Total", 4, 1);
			testPivotTableCellTextEquals(pivotTablePage, "15980.857", 4, 2);
		}
	}		
	
	@Test
	public void testPivotTableRenderingWithSumOfPerUnitPriceValuesPerSalesPersonAsRowWithRegularExpression() {
		String page = "Pivot+-+Sum+of+Per+Unit+Price+per+Sales+Person+with+Regular+Expression";
		PivotTablePage pivotTablePage = testPivotTableCellTextEquals(page, "Sales Person", 1, 1);		
		testPivotTableCellTextEquals(pivotTablePage, "Sum of Per Unit Price", 1, 2);
		testPivotTableCellTextEquals(pivotTablePage, "Imran", 2, 1);
		testPivotTableCellTextEquals(pivotTablePage, "3599.01", 2, 2);
		testPivotTableCellTextEquals(pivotTablePage, "Larry", 3, 1);
		testPivotTableCellTextEquals(pivotTablePage, "37708.5", 3, 2);
	}		
	
	// Pivot - Values - Columns
	@Test
	public void testPivotTableRenderingWithCountOfPerUnitPriceValuesPerSalesPersonAsColumn() {
		String page = "Pivot+-+Column+-+Count+of+Per+Unit+Price+per+Sales+Person";
		testPivotTableRenderingWithCountOfPerUnitPriceValuesPerSalesPersonAsColumn(page, false);
	}		
	
	@Test
	public void testPivotTableRenderingWithCountOfPerUnitPriceValuesPerSalesPersonAsColumnWithTotalsForRows() {
		String page = "Pivot+-+Column+-+Count+of+Per+Unit+Price+per+Sales+Person+with+totals+for+rows";
		testPivotTableRenderingWithCountOfPerUnitPriceValuesPerSalesPersonAsColumn(page, true);
	}		
	
	private void testPivotTableRenderingWithCountOfPerUnitPriceValuesPerSalesPersonAsColumn(String page, boolean totalsForRows) {
		PivotTablePage pivotTablePage = testPivotTableCellTextEquals(page, "Sales Person", 1, 1);		
		testPivotTableCellTextEquals(pivotTablePage, "Imran", 1, 2);
		testPivotTableCellTextEquals(pivotTablePage, "Larry", 1, 3);
		testPivotTableCellTextEquals(pivotTablePage, "Count of Per Unit Price", 2, 1);
		testPivotTableCellTextEquals(pivotTablePage, "4", 2, 2);
		testPivotTableCellTextEquals(pivotTablePage, "3", 2, 3);
		if (totalsForRows) {
			testPivotTableCellTextEquals(pivotTablePage, "Grand Total", 1, 4);
			testPivotTableCellTextEquals(pivotTablePage, "7", 2, 4);			
		}
	}		
	
	@Test
	public void testPivotTableRenderingWithSumOfPerUnitPriceValuesPerSalesPersonAsColumn() {
		String page = "Pivot+-+Column+-+Sum+of+Per+Unit+Price+per+Sales+Person";
		testPivotTableRenderingWithSumOfPerUnitPriceValuesPerSalesPersonAsColumn(page, false);
	}		

	@Test
	public void testPivotTableRenderingWithSumOfPerUnitPriceValuesPerSalesPersonAsColumnWithTotalsForRows() {
		String page = "Pivot+-+Column+-+Sum+of+Per+Unit+Price+per+Sales+Person+with+totals+for+rows";
		testPivotTableRenderingWithSumOfPerUnitPriceValuesPerSalesPersonAsColumn(page, true);
	}		

	private void testPivotTableRenderingWithSumOfPerUnitPriceValuesPerSalesPersonAsColumn(String page, boolean totalsForRows) {
		PivotTablePage pivotTablePage = testPivotTableCellTextEquals(page, "Sales Person", 1, 1);		
		testPivotTableCellTextEquals(pivotTablePage, "Imran", 1, 2);
		testPivotTableCellTextEquals(pivotTablePage, "Larry", 1, 3);
		testPivotTableCellTextEquals(pivotTablePage, "Sum of Per Unit Price", 2, 1);
		testPivotTableCellTextEquals(pivotTablePage, "49858", 2, 2);
		testPivotTableCellTextEquals(pivotTablePage, "62008", 2, 3);
		if (totalsForRows) {
			testPivotTableCellTextEquals(pivotTablePage, "Grand Total", 1, 4);
			testPivotTableCellTextEquals(pivotTablePage, "111866", 2, 4);			
		}
	}		

	@Test
	public void testPivotTableRenderingWithAverageOfPerUnitPriceValuesPerSalesPersonAsColumn() {
		String page = "Pivot+-+Column+-+Average+of+Per+Unit+Price+per+Sales+Person";
		testPivotTableRenderingWithAverageOfPerUnitPriceValuesPerSalesPersonAsColumn(page, false);
	}		

	@Test
	public void testPivotTableRenderingWithAverageOfPerUnitPriceValuesPerSalesPersonAsColumnWithTotalsForRows() {
		String page = "Pivot+-+Column+-+Average+of+Per+Unit+Price+per+Sales+Person+with+totals+for+rows";
		testPivotTableRenderingWithAverageOfPerUnitPriceValuesPerSalesPersonAsColumn(page, true);
	}		

	private void testPivotTableRenderingWithAverageOfPerUnitPriceValuesPerSalesPersonAsColumn(String page, boolean totalsForRows) {
		PivotTablePage pivotTablePage = testPivotTableCellTextEquals(page, "Sales Person", 1, 1);		
		testPivotTableCellTextEquals(pivotTablePage, "Imran", 1, 2);
		testPivotTableCellTextEquals(pivotTablePage, "Larry", 1, 3);
		testPivotTableCellTextEquals(pivotTablePage, "Average of Per Unit Price", 2, 1);
		testPivotTableCellTextEquals(pivotTablePage, "12464.5", 2, 2);
		testPivotTableCellTextEquals(pivotTablePage, "20669.334", 2, 3);
		if (totalsForRows) {
			testPivotTableCellTextEquals(pivotTablePage, "Grand Total", 1, 4);
			testPivotTableCellTextEquals(pivotTablePage, "15980.857", 2, 4);			
		}
	}

	// Pivot - Values - Rows - Columns
	@Test
	public void testPivotTableRenderingWithCountOfUnitsSoldValuesPerSalesPersonPerColorOfItem() {
		String page = "Pivot+-+Count+of+Units+Sold+per+Sales+Person+per+Color+of+Item";
		testPivotTableRenderingWithCountOfUnitsSoldValuesPerSalesPersonPerColorOfItem(page, false, false);
	}		

	@Test
	public void testPivotTableRenderingWithCountOfUnitsSoldValuesPerSalesPersonPerColorOfItemWithTotalsForRowsAndColumns() {
		String page = "Pivot+-+Count+of+Units+Sold+per+Sales+Person+per+Color+of+Item+with+Totals+for+rows+and+columns";
		testPivotTableRenderingWithCountOfUnitsSoldValuesPerSalesPersonPerColorOfItem(page, true, true);
	}

	@Test
	public void testPivotTableRenderingWithCountOfUnitsSoldValuesPerSalesPersonPerColorOfItemWithTotalsForRows() {
		String page = "Pivot+-+Count+of+Units+Sold+per+Sales+Person+per+Color+of+Item+with+Totals+for+rows";
		testPivotTableRenderingWithCountOfUnitsSoldValuesPerSalesPersonPerColorOfItem(page, true, false);
	}

	@Test
	public void testPivotTableRenderingWithCountOfUnitsSoldValuesPerSalesPersonPerColorOfItemWithTotalsForColumns() {
		String page = "Pivot+-+Count+of+Units+Sold+per+Sales+Person+per+Color+of+Item+with+Totals+for+columns";
		testPivotTableRenderingWithCountOfUnitsSoldValuesPerSalesPersonPerColorOfItem(page, false, true);
	}

	private void testPivotTableRenderingWithCountOfUnitsSoldValuesPerSalesPersonPerColorOfItem(String page, boolean totalsForRows, boolean totalsForColumns) {
		PivotTablePage pivotTablePage = testPivotTableCellTextEquals(page, "Count of Units Sold", 1, 1);		
		testPivotTableCellTextEquals(pivotTablePage, "Black", 1, 2);
		testPivotTableCellTextEquals(pivotTablePage, "Red", 1, 3);
		testPivotTableCellTextEquals(pivotTablePage, "White", 1, 4);
		testPivotTableCellTextEquals(pivotTablePage, "Imran", 2, 1);
		testPivotTableCellTextEquals(pivotTablePage, "2", 2, 2);
		testPivotTableCellTextEquals(pivotTablePage, "1", 2, 3);
		testPivotTableCellTextEquals(pivotTablePage, "1", 2, 4);
		testPivotTableCellTextEquals(pivotTablePage, "Larry", 3, 1);
		testPivotTableCellTextEquals(pivotTablePage, "1", 3, 2);
		testPivotTableCellTextEquals(pivotTablePage, "1", 3, 3);
		testPivotTableCellTextEquals(pivotTablePage, "1", 3, 4);
		if (totalsForRows) {
			testPivotTableCellTextEquals(pivotTablePage, "Grand Total", 1, 5);
			testPivotTableCellTextEquals(pivotTablePage, "4", 2, 5);
			testPivotTableCellTextEquals(pivotTablePage, "3", 3, 5);
		}
		if (totalsForColumns) {
			testPivotTableCellTextEquals(pivotTablePage, "Grand Total", 4, 1);
			testPivotTableCellTextEquals(pivotTablePage, "3", 4, 2);
			testPivotTableCellTextEquals(pivotTablePage, "2", 4, 3);
			testPivotTableCellTextEquals(pivotTablePage, "2", 4, 4);
		}
		if (totalsForRows && totalsForColumns) {
			testPivotTableCellTextEquals(pivotTablePage, "7", 4, 5);
		}
	}		

	@Test
	public void testPivotTableRenderingWithSumOfPerUnitPriceValuesPerSalesPersonPerColorOfItem() {
		String page = "Pivot+-+Sum+of+Per+Unit+Price+per+Sales+Person+per+Color+of+Item";
		testPivotTableRenderingWithSumOfPerUnitPriceValuesPerSalesPersonPerColorOfItem(page, false, false);
	}		

	@Test
	public void testPivotTableRenderingWithSumOfPerUnitPriceValuesPerSalesPersonPerColorOfItemWithTotalsForRowsAndColumns() {
		String page = "Pivot+-+Sum+of+Per+Unit+Price+per+Sales+Person+per+Color+of+Item+with+Totals+for+rows+and+columns";
		testPivotTableRenderingWithSumOfPerUnitPriceValuesPerSalesPersonPerColorOfItem(page, true, true);
	}

	@Test
	public void testPivotTableRenderingWithSumOfPerUnitPriceValuesPerSalesPersonPerColorOfItemWithBlankRows() {
		String page = "Pivot+-+Sum+of+Per+Unit+Price+per+Sales+Person+per+Color+of+Item+with+Blank+Rows";
		testPivotTableRenderingWithSumOfPerUnitPriceValuesPerSalesPersonPerColorOfItemWithBlankRows(page, false, false);
	}

	@Test
	public void testPivotTableRenderingWithSumOfPerUnitPriceValuesPerSalesPersonPerColorOfItemWithTotalsForRowsWithBlankRows() {
		String page = "Pivot+-+Sum+of+Per+Unit+Price+per+Sales+Person+per+Color+of+Item+with+Totals+for+rows+with+Blank+Rows";
		testPivotTableRenderingWithSumOfPerUnitPriceValuesPerSalesPersonPerColorOfItemWithBlankRows(page, true, false);
	}

	@Test
	public void testPivotTableRenderingWithSumOfPerUnitPriceValuesPerSalesPersonPerColorOfItemWithTotalsForColumnsWithBlankRows() {
		String page = "Pivot+-+Sum+of+Per+Unit+Price+per+Sales+Person+per+Color+of+Item+with+Totals+for+columns+with+Blank+Rows";
		testPivotTableRenderingWithSumOfPerUnitPriceValuesPerSalesPersonPerColorOfItemWithBlankRows(page, false, true);
	}

	@Test
	public void testPivotTableRenderingWithSumOfPerUnitPriceValuesPerSalesPersonPerColorOfItemWithTotalsForRowsAndColumnsWithBlankRows() {
		String page = "Pivot+-+Sum+of+Per+Unit+Price+per+Sales+Person+per+Color+of+Item+with+Totals+for+rows+and+columns+with+Blank+Rows";
		testPivotTableRenderingWithSumOfPerUnitPriceValuesPerSalesPersonPerColorOfItemWithBlankRows(page, true, true);
	}

	@Test
	public void testPivotTableRenderingWithSumOfPerUnitPriceValuesPerSalesPersonPerColorOfItemWithBlankColumns() {
		String page = "Pivot+-+Sum+of+Per+Unit+Price+per+Sales+Person+per+Color+of+Item+with+Blank+Columns";
		testPivotTableRenderingWithSumOfPerUnitPriceValuesPerSalesPersonPerColorOfItemWithBlankColumns(page, false, false);
	}

	@Test
	public void testPivotTableRenderingWithSumOfPerUnitPriceValuesPerSalesPersonPerColorOfItemWithTotalsForRowsWithBlankColumns() {
		String page = "Pivot+-+Sum+of+Per+Unit+Price+per+Sales+Person+per+Color+of+Item+with+Totals+for+rows+with+Blank+Columns";
		testPivotTableRenderingWithSumOfPerUnitPriceValuesPerSalesPersonPerColorOfItemWithBlankColumns(page, true, false);
	}

	@Test
	public void testPivotTableRenderingWithSumOfPerUnitPriceValuesPerSalesPersonPerColorOfItemWithTotalsForColumnsWithBlankColumns() {
		String page = "Pivot+-+Sum+of+Per+Unit+Price+per+Sales+Person+per+Color+of+Item+with+Totals+for+columns+with+Blank+Columns";
		testPivotTableRenderingWithSumOfPerUnitPriceValuesPerSalesPersonPerColorOfItemWithBlankColumns(page, false, true);
	}

	@Test
	public void testPivotTableRenderingWithSumOfPerUnitPriceValuesPerSalesPersonPerColorOfItemWithTotalsForRowsAndColumnsWithBlankColumns() {
		String page = "Pivot+-+Sum+of+Per+Unit+Price+per+Sales+Person+per+Color+of+Item+with+Totals+for+rows+and+columns+with+Blank+Columns";
		testPivotTableRenderingWithSumOfPerUnitPriceValuesPerSalesPersonPerColorOfItemWithBlankColumns(page, true, true);
	}

	@Test
	public void testPivotTableRenderingWithSumOfPerUnitPriceValuesPerSalesPersonPerColorOfItemWithBlankRowsAndColumns() {
		String page = "Pivot+-+Sum+of+Per+Unit+Price+per+Sales+Person+per+Color+of+Item+with+Blank+Rows+and+Columns";
		testPivotTableRenderingWithSumOfPerUnitPriceValuesPerSalesPersonPerColorOfItemWithBlankRowsAndColumns(page, false, false);
	}

	@Test
	public void testPivotTableRenderingWithSumOfPerUnitPriceValuesPerSalesPersonPerColorOfItemWithTotalsForRowsWithBlankRowsAndColumns() {
		String page = "Pivot+-+Sum+of+Per+Unit+Price+per+Sales+Person+per+Color+of+Item+with+Totals+for+rows+with+Blank+Rows+and+Columns";
		testPivotTableRenderingWithSumOfPerUnitPriceValuesPerSalesPersonPerColorOfItemWithBlankRowsAndColumns(page, true, false);
	}

	@Test
	public void testPivotTableRenderingWithSumOfPerUnitPriceValuesPerSalesPersonPerColorOfItemWithTotalsForColumnsWithBlankRowsAndColumns() {
		String page = "Pivot+-+Sum+of+Per+Unit+Price+per+Sales+Person+per+Color+of+Item+with+Totals+for+columns+with+Blank+Rows+and+Columns";
		testPivotTableRenderingWithSumOfPerUnitPriceValuesPerSalesPersonPerColorOfItemWithBlankRowsAndColumns(page, false, true);
	}

	@Test
	public void testPivotTableRenderingWithSumOfPerUnitPriceValuesPerSalesPersonPerColorOfItemWithTotalsForRowsAndColumnsWithBlankRowsAndColumns() {
		String page = "Pivot+-+Sum+of+Per+Unit+Price+per+Sales+Person+per+Color+of+Item+with+Totals+for+rows+and+columns+with+Blank+Rows+and+Columns";
		testPivotTableRenderingWithSumOfPerUnitPriceValuesPerSalesPersonPerColorOfItemWithBlankRowsAndColumns(page, true, true);
	}

	@Test
	public void testPivotTableRenderingWithSumOfPerUnitPriceValuesPerSalesPersonPerColorOfItemWithTotalsForRows() {
		String page = "Pivot+-+Sum+of+Per+Unit+Price+per+Sales+Person+per+Color+of+Item+with+Totals+for+rows";
		testPivotTableRenderingWithSumOfPerUnitPriceValuesPerSalesPersonPerColorOfItem(page, true, false);
	}
	
	@Test
	public void testPivotTableRenderingWithSumOfPerUnitPriceValuesPerSalesPersonPerColorOfItemWithTotalsForColumns() {
		String page = "Pivot+-+Sum+of+Per+Unit+Price+per+Sales+Person+per+Color+of+Item+with+Totals+for+columns";
		testPivotTableRenderingWithSumOfPerUnitPriceValuesPerSalesPersonPerColorOfItem(page, false, true);
	}
	
	private void testPivotTableRenderingWithSumOfPerUnitPriceValuesPerSalesPersonPerColorOfItem(
			String page, boolean totalsForRows, boolean totalsForColumns) {
		PivotTablePage pivotTablePage = testPivotTableCellTextEquals(page, "Sum of Per Unit Price", 1, 1);		
		testPivotTableCellTextEquals(pivotTablePage, "Black", 1, 2);
		testPivotTableCellTextEquals(pivotTablePage, "Red", 1, 3);
		testPivotTableCellTextEquals(pivotTablePage, "White", 1, 4);
		testPivotTableCellTextEquals(pivotTablePage, "Imran", 2, 1);
		testPivotTableCellTextEquals(pivotTablePage, "49000", 2, 2);
		testPivotTableCellTextEquals(pivotTablePage, "850", 2, 3);
		testPivotTableCellTextEquals(pivotTablePage, "8", 2, 4);
		testPivotTableCellTextEquals(pivotTablePage, "Larry", 3, 1);
		testPivotTableCellTextEquals(pivotTablePage, "8", 3, 2);
		testPivotTableCellTextEquals(pivotTablePage, "35000", 3, 3);
		testPivotTableCellTextEquals(pivotTablePage, "27000", 3, 4);
		if (totalsForRows) {
			testPivotTableCellTextEquals(pivotTablePage, "Grand Total", 1, 5);
			testPivotTableCellTextEquals(pivotTablePage, "49858", 2, 5);
			testPivotTableCellTextEquals(pivotTablePage, "62008", 3, 5);
		}
		if (totalsForColumns) {
			testPivotTableCellTextEquals(pivotTablePage, "Grand Total", 4, 1);
			testPivotTableCellTextEquals(pivotTablePage, "49008", 4, 2);
			testPivotTableCellTextEquals(pivotTablePage, "35850", 4, 3);
			testPivotTableCellTextEquals(pivotTablePage, "27008", 4, 4);			
		}
		if (totalsForRows && totalsForColumns) {
			testPivotTableCellTextEquals(pivotTablePage, "111866", 4, 5);
		}
	}

	private void testPivotTableRenderingWithSumOfPerUnitPriceValuesPerSalesPersonPerColorOfItemWithBlankRows(
			String page, boolean totalsForRows, boolean totalsForColumns) {
		PivotTablePage pivotTablePage = testPivotTableCellTextEquals(page, "Sum of Per Unit Price", 1, 1);		
		testPivotTableCellTextEquals(pivotTablePage, "Black", 1, 2);
		testPivotTableCellTextEquals(pivotTablePage, "Red", 1, 3);
		testPivotTableCellTextEquals(pivotTablePage, "White", 1, 4);
		testPivotTableCellTextEquals(pivotTablePage, "Imran", 2, 1);
		testPivotTableCellTextEquals(pivotTablePage, "24000", 2, 2);
		testPivotTableCellTextEquals(pivotTablePage, "850", 2, 3);
		testPivotTableCellTextEquals(pivotTablePage, "8", 2, 4);
		testPivotTableCellTextEquals(pivotTablePage, "Larry", 3, 1);
		testPivotTableCellTextEquals(pivotTablePage, "8", 3, 2);
		testPivotTableCellTextEquals(pivotTablePage, "35000", 3, 3);
		testPivotTableCellTextEquals(pivotTablePage, "27000", 3, 4);
		testPivotTableCellTextEquals(pivotTablePage, "(Blanks)", 4, 1);
		testPivotTableCellTextEquals(pivotTablePage, "25000", 4, 2);
		testPivotTableCellTextEquals(pivotTablePage, "", 4, 3);
		testPivotTableCellTextEquals(pivotTablePage, "", 4, 4);
		if (totalsForRows) {
			testPivotTableCellTextEquals(pivotTablePage, "Grand Total", 1, 5);
			testPivotTableCellTextEquals(pivotTablePage, "24858", 2, 5);
			testPivotTableCellTextEquals(pivotTablePage, "62008", 3, 5);
			testPivotTableCellTextEquals(pivotTablePage, "25000", 4, 5);
		}
		if (totalsForColumns) {
			testPivotTableCellTextEquals(pivotTablePage, "Grand Total", 5, 1);
			testPivotTableCellTextEquals(pivotTablePage, "49008", 5, 2);
			testPivotTableCellTextEquals(pivotTablePage, "35850", 5, 3);
			testPivotTableCellTextEquals(pivotTablePage, "27008", 5, 4);			
		}
		if (totalsForRows && totalsForColumns) {
			testPivotTableCellTextEquals(pivotTablePage, "111866", 5, 5);
		}
	}

	private void testPivotTableRenderingWithSumOfPerUnitPriceValuesPerSalesPersonPerColorOfItemWithBlankColumns(
			String page, boolean totalsForRows, boolean totalsForColumns) {
		PivotTablePage pivotTablePage = testPivotTableCellTextEquals(page, "Sum of Per Unit Price", 1, 1);		
		testPivotTableCellTextEquals(pivotTablePage, "Black", 1, 2);
		testPivotTableCellTextEquals(pivotTablePage, "Red", 1, 3);
		testPivotTableCellTextEquals(pivotTablePage, "White", 1, 4);
		testPivotTableCellTextEquals(pivotTablePage, "(Blanks)", 1, 5);
		testPivotTableCellTextEquals(pivotTablePage, "Imran", 2, 1);
		testPivotTableCellTextEquals(pivotTablePage, "49000", 2, 2);
		testPivotTableCellTextEquals(pivotTablePage, "850", 2, 3);
		testPivotTableCellTextEquals(pivotTablePage, "8", 2, 4);
		testPivotTableCellTextEquals(pivotTablePage, "", 2, 5);
		testPivotTableCellTextEquals(pivotTablePage, "Larry", 3, 1);
		testPivotTableCellTextEquals(pivotTablePage, "8", 3, 2);
		testPivotTableCellTextEquals(pivotTablePage, "", 3, 3);
		testPivotTableCellTextEquals(pivotTablePage, "27000", 3, 4);
		testPivotTableCellTextEquals(pivotTablePage, "35000", 3, 5);
		if (totalsForRows) {
			testPivotTableCellTextEquals(pivotTablePage, "Grand Total", 1, 6);
			testPivotTableCellTextEquals(pivotTablePage, "49858", 2, 6);
			testPivotTableCellTextEquals(pivotTablePage, "62008", 3, 6);
		}
		if (totalsForColumns) {
			testPivotTableCellTextEquals(pivotTablePage, "Grand Total", 4, 1);
			testPivotTableCellTextEquals(pivotTablePage, "49008", 4, 2);
			testPivotTableCellTextEquals(pivotTablePage, "850", 4, 3);
			testPivotTableCellTextEquals(pivotTablePage, "27008", 4, 4);			
			testPivotTableCellTextEquals(pivotTablePage, "35000", 4, 5);
		}
		if (totalsForRows && totalsForColumns) {
			testPivotTableCellTextEquals(pivotTablePage, "111866", 4, 6);
		}
	}

	private void testPivotTableRenderingWithSumOfPerUnitPriceValuesPerSalesPersonPerColorOfItemWithBlankRowsAndColumns(
			String page, boolean totalsForRows, boolean totalsForColumns) {
		PivotTablePage pivotTablePage = testPivotTableCellTextEquals(page, "Sum of Per Unit Price", 1, 1);		
		testPivotTableCellTextEquals(pivotTablePage, "Black", 1, 2);
		testPivotTableCellTextEquals(pivotTablePage, "Red", 1, 3);
		testPivotTableCellTextEquals(pivotTablePage, "White", 1, 4);
		testPivotTableCellTextEquals(pivotTablePage, "(Blanks)", 1, 5);
		testPivotTableCellTextEquals(pivotTablePage, "Imran", 2, 1);
		testPivotTableCellTextEquals(pivotTablePage, "49000", 2, 2);
		testPivotTableCellTextEquals(pivotTablePage, "850", 2, 3);
		testPivotTableCellTextEquals(pivotTablePage, "", 2, 4);
		testPivotTableCellTextEquals(pivotTablePage, "8", 2, 5);
		testPivotTableCellTextEquals(pivotTablePage, "Larry", 3, 1);
		testPivotTableCellTextEquals(pivotTablePage, "", 3, 2);
		testPivotTableCellTextEquals(pivotTablePage, "", 3, 3);
		testPivotTableCellTextEquals(pivotTablePage, "62000", 3, 4);
		testPivotTableCellTextEquals(pivotTablePage, "", 3, 5);
		testPivotTableCellTextEquals(pivotTablePage, "(Blanks)", 4, 1);
		testPivotTableCellTextEquals(pivotTablePage, "8", 4, 2);
		testPivotTableCellTextEquals(pivotTablePage, "", 4, 3);
		testPivotTableCellTextEquals(pivotTablePage, "", 4, 4);
		testPivotTableCellTextEquals(pivotTablePage, "", 4, 5);

		if (totalsForRows) {
			testPivotTableCellTextEquals(pivotTablePage, "Grand Total", 1, 6);
			testPivotTableCellTextEquals(pivotTablePage, "49858", 2, 6);
			testPivotTableCellTextEquals(pivotTablePage, "62000", 3, 6);
			testPivotTableCellTextEquals(pivotTablePage, "8", 4, 6);
		}
		if (totalsForColumns) {
			testPivotTableCellTextEquals(pivotTablePage, "Grand Total", 5, 1);
			testPivotTableCellTextEquals(pivotTablePage, "49008", 5, 2);
			testPivotTableCellTextEquals(pivotTablePage, "850", 5, 3);
			testPivotTableCellTextEquals(pivotTablePage, "62000", 5, 4);			
			testPivotTableCellTextEquals(pivotTablePage, "8", 5, 5);
		}
		if (totalsForRows && totalsForColumns) {
			testPivotTableCellTextEquals(pivotTablePage, "111866", 5, 6);
		}
	}

	@Test
	public void testPivotTableRenderingWithAverageOfPerUnitPriceValuesPerSalesPersonPerColorOfItem() {
		String page = "Pivot+-+Average+of+Per+Unit+Price+per+Sales+Person+per+Color+of+Item";
		testPivotTableRenderingWithAverageOfPerUnitPriceValuesPerSalesPersonPerColorOfItem(page, false, false);
	}		

	@Test
	public void testPivotTableRenderingWithAverageOfPerUnitPriceValuesPerSalesPersonPerColorOfItemWithTotalsForRowsAndColumns() {
		String page = "Pivot+-+Average+of+Per+Unit+Price+per+Sales+Person+per+Color+of+Item+with+totals+for+rows+and+columns";
		testPivotTableRenderingWithAverageOfPerUnitPriceValuesPerSalesPersonPerColorOfItem(page, true, true);
	}

	@Test
	public void testPivotTableRenderingWithAverageOfPerUnitPriceValuesPerSalesPersonPerColorOfItemWithTotalsForRows() {
		String page = "Pivot+-+Average+of+Per+Unit+Price+per+Sales+Person+per+Color+of+Item+with+totals+for+rows";
		testPivotTableRenderingWithAverageOfPerUnitPriceValuesPerSalesPersonPerColorOfItem(page, true, false);
	}

	@Test
	public void testPivotTableRenderingWithAverageOfPerUnitPriceValuesPerSalesPersonPerColorOfItemWithTotalsForColumns() {
		String page = "Pivot+-+Average+of+Per+Unit+Price+per+Sales+Person+per+Color+of+Item+with+totals+for+columns";
		testPivotTableRenderingWithAverageOfPerUnitPriceValuesPerSalesPersonPerColorOfItem(page, false, true);
	}

	private void testPivotTableRenderingWithAverageOfPerUnitPriceValuesPerSalesPersonPerColorOfItem(
			String page, boolean totalsForRows, boolean totalsForColumns) {
		PivotTablePage pivotTablePage = testPivotTableCellTextEquals(page, "Average of Per Unit Price", 1, 1);		
		testPivotTableCellTextEquals(pivotTablePage, "Black", 1, 2);
		testPivotTableCellTextEquals(pivotTablePage, "Red", 1, 3);
		testPivotTableCellTextEquals(pivotTablePage, "White", 1, 4);
		testPivotTableCellTextEquals(pivotTablePage, "Imran", 2, 1);
		testPivotTableCellTextEquals(pivotTablePage, "24500", 2, 2);
		testPivotTableCellTextEquals(pivotTablePage, "850", 2, 3);
		testPivotTableCellTextEquals(pivotTablePage, "8", 2, 4);
		testPivotTableCellTextEquals(pivotTablePage, "Larry", 3, 1);
		testPivotTableCellTextEquals(pivotTablePage, "8", 3, 2);
		testPivotTableCellTextEquals(pivotTablePage, "35000", 3, 3);
		testPivotTableCellTextEquals(pivotTablePage, "27000", 3, 4);
		if (totalsForRows) {
			testPivotTableCellTextEquals(pivotTablePage, "Grand Total", 1, 5);
			testPivotTableCellTextEquals(pivotTablePage, "12464.5", 2, 5);
			testPivotTableCellTextEquals(pivotTablePage, "20669.334", 3, 5);
		}
		if (totalsForColumns) {			
			testPivotTableCellTextEquals(pivotTablePage, "Grand Total", 4, 1);
			testPivotTableCellTextEquals(pivotTablePage, "16336", 4, 2);
			testPivotTableCellTextEquals(pivotTablePage, "17925", 4, 3);
			testPivotTableCellTextEquals(pivotTablePage, "13504", 4, 4);
		}
		if (totalsForRows && totalsForColumns) {
			testPivotTableCellTextEquals(pivotTablePage, "15980.857", 4, 5);
		}
	}	
	
	@Test
	@Ignore
	public void testPivotTableRenderingWithStarOfCountUnitsSoldPerSalesPersonPerColorOfItem() {
		String page = "Pivot+-+Stars+of+Count+of+Units+Sold+per+Sales+Person+per+Color+of+Item";
		testPivotTableRenderingWithStarsOfTotalPriceValuesPerSalesPersonPerColorOfItem(page, false, false);
	}		

	@Test
	@Ignore
	public void testPivotTableRenderingWithStarOfCountUnitsSoldPerSalesPersonPerColorOfItemWithTotalsForRowsAndColumns() {
		String page = "Pivot+-+Stars+of+Count+of+Units+Sold+per+Sales+Person+per+Color+of+Item+with+totals+for+rows+and+columns";
		testPivotTableRenderingWithStarsOfTotalPriceValuesPerSalesPersonPerColorOfItem(page, true, true);
	}

	private void testPivotTableRenderingWithStarsOfTotalPriceValuesPerSalesPersonPerColorOfItem(
			String page, boolean totalsForRows, boolean totalsForColumns) {
		String startsWith = "<img class=\"emoticon emoticon-yellow-star\" src=\"";
		String endsWith = "/images/icons/emoticons/star_yellow.png\" data-emoticon-name=\"yellow-star\" alt=\"(star)\" />";
		PivotTablePage pivotTablePage = testPivotTableCellTextEquals(page, "Units Sold", 1, 1);	
		testPivotTableCellTextEquals(pivotTablePage, "Black", 1, 2);
		testPivotTableCellTextEquals(pivotTablePage, "Red", 1, 3);
		testPivotTableCellTextEquals(pivotTablePage, "White", 1, 4);
		testPivotTableCellTextEquals(pivotTablePage, "Imran", 2, 1);
		testPivotTableCellTextStartsWith(pivotTablePage, startsWith, 2, 2);
		testPivotTableCellTextEndsWith(pivotTablePage, endsWith, 2, 2);
		testPivotTableCellTextStartsWith(pivotTablePage, startsWith, 2, 3);
		testPivotTableCellTextEndsWith(pivotTablePage, endsWith, 2, 3);
		testPivotTableCellTextStartsWith(pivotTablePage, startsWith, 2, 4);
		testPivotTableCellTextEndsWith(pivotTablePage, endsWith, 2, 4);
		testPivotTableCellTextEquals(pivotTablePage, "Larry", 3, 1);
		testPivotTableCellTextStartsWith(pivotTablePage, startsWith, 3, 2);
		testPivotTableCellTextEndsWith(pivotTablePage, endsWith, 3, 2);
		testPivotTableCellTextStartsWith(pivotTablePage, startsWith, 3, 3);
		testPivotTableCellTextEndsWith(pivotTablePage, endsWith, 3, 3);
		testPivotTableCellTextStartsWith(pivotTablePage, startsWith, 3, 4);
		testPivotTableCellTextEndsWith(pivotTablePage, endsWith, 3, 4);
		if (totalsForRows) {
			testPivotTableCellTextEquals(pivotTablePage, "Grand Total", 1, 5);
			testPivotTableCellTextStartsWith(pivotTablePage, startsWith, 2, 5);
			testPivotTableCellTextEndsWith(pivotTablePage, endsWith, 2, 5);
			testPivotTableCellTextStartsWith(pivotTablePage, startsWith, 3, 5);
			testPivotTableCellTextEndsWith(pivotTablePage, endsWith, 3, 5);
		}
		if (totalsForColumns) {			
			testPivotTableCellTextEquals(pivotTablePage, "Grand Total", 4, 1);
			testPivotTableCellTextStartsWith(pivotTablePage, startsWith, 4, 2);
			testPivotTableCellTextEndsWith(pivotTablePage, endsWith, 4, 2);
			testPivotTableCellTextStartsWith(pivotTablePage, startsWith, 4, 3);
			testPivotTableCellTextEndsWith(pivotTablePage, endsWith, 4, 3);
			testPivotTableCellTextStartsWith(pivotTablePage, startsWith, 4, 4);
			testPivotTableCellTextEndsWith(pivotTablePage, endsWith, 4, 4);
		}
		if (totalsForRows && totalsForColumns) {
			testPivotTableCellTextStartsWith(pivotTablePage, startsWith, 4, 5);
			testPivotTableCellTextEndsWith(pivotTablePage, endsWith, 4, 5);
		}
	}	
	
	@Test
	public void testPivotTableRenderingWithListOfCountUnitsSoldPerSalesPersonPerColorOfItem() {
		String page = "Pivot+-+List+of+Units+Sold+per+Sales+Person+per+Color+of+Item";
		testPivotTableRenderingWithListOfUnitSoldValuesPerSalesPersonPerColorOfItem(page, false, false);
	}		

	@Test
	public void testPivotTableRenderingWithListOfCountUnitsSoldPerSalesPersonPerColorOfItemWithTotalsForRowsAndColumns() {
		String page = "Pivot+-+List+of+Units+Sold+per+Sales+Person+per+Color+of+Item+with+totals+for+rows+and+columns";
		testPivotTableRenderingWithListOfUnitSoldValuesPerSalesPersonPerColorOfItem(page, true, true);
	}

	@Test
	@Ignore
	public void testPivotTableRenderingWithListOfCountUnitsSoldPerSalesPersonPerColorOfItemWithTotalsForRows() {
		String page = "Pivot+-+List+of+Units+Sold+per+Sales+Person+per+Color+of+Item+with+totals+for+rows";
		testPivotTableRenderingWithListOfUnitSoldValuesPerSalesPersonPerColorOfItem(page, true, false);
	}

	@Test
	@Ignore
	public void testPivotTableRenderingWithListOfCountUnitsSoldPerSalesPersonPerColorOfItemWithTotalsForColumns() {
		String page = "Pivot+-+List+of+Units+Sold+per+Sales+Person+per+Color+of+Item+with+totals+for+columns";
		testPivotTableRenderingWithListOfUnitSoldValuesPerSalesPersonPerColorOfItem(page, false, true);
	}

	private void testPivotTableRenderingWithListOfUnitSoldValuesPerSalesPersonPerColorOfItem(
			String page, boolean totalsForRows, boolean totalsForColumns) {
		PivotTablePage pivotTablePage = testPivotTableCellTextEquals(page, "Units Sold", 1, 1);		
		testPivotTableCellTextEquals(pivotTablePage, "Black", 1, 2);
		testPivotTableCellTextEquals(pivotTablePage, "Red", 1, 3);
		testPivotTableCellTextEquals(pivotTablePage, "White", 1, 4);
		testPivotTableCellTextEquals(pivotTablePage, "Imran", 2, 1);
		testPivotTableCellTextEquals(pivotTablePage, "8, 18", 2, 2);
		testPivotTableCellTextEquals(pivotTablePage, "6", 2, 3);
		testPivotTableCellTextEquals(pivotTablePage, "200, 32", 2, 4);
		testPivotTableCellTextEquals(pivotTablePage, "Larry", 3, 1);
		testPivotTableCellTextEquals(pivotTablePage, "4", 3, 2);
		testPivotTableCellTextEquals(pivotTablePage, "4", 3, 3);
		testPivotTableCellTextEquals(pivotTablePage, "10", 3, 4);
		if (totalsForRows) {
			testPivotTableCellTextEquals(pivotTablePage, "Grand Total", 1, 5);
			testPivotTableCellTextEquals(pivotTablePage, "8, 18, 6, 200, 32", 2, 5);
			testPivotTableCellTextEquals(pivotTablePage, "4, 10, 4", 3, 5);
		}
		if (totalsForColumns) {			
			testPivotTableCellTextEquals(pivotTablePage, "Grand Total", 4, 1);
			testPivotTableCellTextEquals(pivotTablePage, "8, 18, 4", 4, 2);
			testPivotTableCellTextEquals(pivotTablePage, "4, 6", 4, 3);
			testPivotTableCellTextEquals(pivotTablePage, "10, 200, 32", 4, 4);
		}
		if (totalsForRows && totalsForColumns) {
			testPivotTableCellTextEquals(pivotTablePage, "8, 18, 6, 200, 32, 4, 10, 4", 4, 5);
		}
	}	
	
	// Pivot - Jira
	@Test
	public void testPivotTableRenderingWithSumOfOrignalEstimateValuesPerPersonAsRowWithRegularExpression() {
		String page = "Pivot+-+Jira+-+Sum+of+Original+Estimate+per+Person+with+Regular+Expression";
		PivotTablePage pivotTablePage = testPivotTableCellTextEquals(page, "Person", 1, 1);		
		testPivotTableCellTextEquals(pivotTablePage, "Sum of Original Estimate", 1, 2);
		testPivotTableCellTextEquals(pivotTablePage, "Imran", 2, 1);
		testPivotTableCellTextEquals(pivotTablePage, "89", 2, 2);
		testPivotTableCellTextEquals(pivotTablePage, "Larry", 3, 1);
		testPivotTableCellTextEquals(pivotTablePage, "508", 3, 2);
	}
	
	@Test
	public void testPivotTableRenderingWithAverageOfOrignalEstimateValuesPerPersonAsRowWithRegularExpression() {
		String page = "Pivot+-+Jira+-+Average+of+Original+Estimate+per+Person+with+Regular+Expression";
		PivotTablePage pivotTablePage = testPivotTableCellTextEquals(page, "Person", 1, 1);		
		testPivotTableCellTextEquals(pivotTablePage, "Average of Original Estimate", 1, 2);
		testPivotTableCellTextEquals(pivotTablePage, "Imran", 2, 1);
		testPivotTableCellTextEquals(pivotTablePage, "22.255", 2, 2);
		testPivotTableCellTextEquals(pivotTablePage, "Larry", 3, 1);
		testPivotTableCellTextEquals(pivotTablePage, "169.36667", 3, 2);
	}
	
	@Test
	public void testNoTableBodyErrorMessage() {
		testErrorMessage("Pivot+-+Error+-+No+table+body", "Please insert a table in the macro body.");
	}
	
	@Test
	public void testWrongRowNameErrorMessage() {
		testErrorMessage("Pivot+-+Error+-+Non-existent+Row", "The column Sales doesn't exist.");
	}
	
	@Test
	public void testWrongColumnNameErrorMessage() {
		testErrorMessage("Pivot+-+Error+-+Non-existent+Column", "The column Item doesn't exist.");
	}
	
	
}
