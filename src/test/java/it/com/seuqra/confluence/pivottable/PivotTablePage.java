package it.com.seuqra.confluence.pivottable;

import org.openqa.selenium.By;

import com.atlassian.confluence.webdriver.pageobjects.page.ConfluenceAbstractPage;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.TimedQuery;


public class PivotTablePage extends ConfluenceAbstractPage {
	protected static final String PREFIX = "/display/PTIT/"; 
	protected static final String PIVOT_TABLE_ID = "pivot";

	// Package visibility because it should be accessible by TestPivotTablePage class
	static String PAGE;

	@ElementBy(id = PIVOT_TABLE_ID)
	protected PageElement pivotTable;

	@ElementBy(id = "main-content")
	protected PageElement mainContent;
	
	public String getUrl() {
		return PREFIX+PAGE;
	}

	TimedQuery<String> geMainContentText() {
		return mainContent.timed().getText();
	}	
	
	TimedQuery<String> getPivotTableCellText(int rowPosition, int columnPosition) {
		String xpath;
		if (rowPosition == 1) {
			xpath = "//thead/tr/th["+columnPosition+"]/div";
		} else {
			if (columnPosition == 1) {
				xpath = "//tbody/tr["+(rowPosition-1)+"]/th["+columnPosition+"]";
			} else {
				xpath = "//tbody/tr["+(rowPosition-1)+"]/td["+(columnPosition-1)+"]";
			}
		}
		return pivotTable.find(By.xpath(xpath)).timed().getText();
	}
	
	TimedQuery<String> getPivotTableId() {
		return pivotTable.timed().getAttribute("id");
	}

}
