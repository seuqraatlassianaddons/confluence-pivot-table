package ut.com.seuqra.confluence.pivottable;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.BasicConfigurator;
import org.junit.BeforeClass;
import org.junit.Test;

import com.seuqra.confluence.pivottable.ColumnValueParameters;
import com.seuqra.confluence.pivottable.PivotTable;
import com.seuqra.confluence.pivottable.Utils;

public class UtilsTest {
	
	//private static final String REGEXP_JIRA = "((\\d*) week){0,1}(s){0,1}(, ){0,1}((\\d*) day){0,1}(s){0,1}(, ){0,1}((\\d*) hour){0,1}(s){0,1}(, ){0,1}";
	//private static final String REGEXP_JIRA = "((\\d*)( ){0,1}(w)){0,1}(eek){0,1}(s){0,1}(,){0,1}( ){0,1}((\\d*)( ){0,1}(d)){0,1}(ay){0,1}(s){0,1}(,){0,1}( ){0,1}((\\d*)( ){0,1}(h)){0,1}(our){0,1}(s){0,1}(,){0,1}( ){0,1}";
	/*
	private static final String REGEXP_JIRA = 
			"((\\d*)( ){0,1}(w)){0,1}(eek){0,1}(s){0,1}(,){0,1}( ){0,1}"
			+ "((\\d*)( ){0,1}(d)){0,1}(ay){0,1}(s){0,1}(,){0,1}( ){0,1}"
			+ "((\\d*)( ){0,1}(h)){0,1}(our){0,1}(s){0,1}(,){0,1}( ){0,1}"
			+ "((\\d*)( ){0,1}(m)){0,1}(inute){0,1}(s){0,1}(,){0,1}( ){0,1}"
			+ "((\\d*)( ){0,1}(s)){0,1}(econd){0,1}(s){0,1}(,){0,1}( ){0,1}";

	private static final String REGEXP_JIRA = 
			"((\\d*)( ){0,1}(w)){0,1}(eek){0,1}(.*){0,1}"
			+ "((\\d*)( ){0,1}(d)){0,1}(ay){0,1}(.*){0,1}"
			+ "((\\d*)( ){0,1}(h)){0,1}(our){0,1}(.*){0,1}"
			+ "((\\d*)( ){0,1}(m)){0,1}(inute){0,1}(s)(.*){0,1}"
			+ "((\\d*)( ){0,1}(s)){0,1}(econd){0,1}(s)(.*){0,1}";
						*/
	static final String REGEXP(char letter) { return "(((\\d*)([\\.|,]){0,1}(\\d*))( ){0,1}("+letter+")){0,1}(\\D*){0,1}";};
	static final String REGEXP_JIRA = REGEXP('w') + REGEXP('d')  + REGEXP('h')  + REGEXP('m')  + REGEXP('s');
/*
	private static final String REGEXP_JIRA = 
			  "(((\\d*)([\\.|,]){0,1}(\\d*))( ){0,1}(w)){0,1}(\\D*){0,1}"
			+ "(((\\d*)([\\.|,]){0,1}(\\d*))( ){0,1}(d)){0,1}(\\D*){0,1}"
			+ "(((\\d*)([\\.|,]){0,1}(\\d*))( ){0,1}(h)){0,1}(\\D*){0,1}"
			+ "(((\\d*)([\\.|,]){0,1}(\\d*))( ){0,1}(m)){0,1}(\\D*){0,1}"
			+ "(((\\d*)([\\.|,]){0,1}(\\d*))( ){0,1}(s)){0,1}(\\D*){0,1}";
			*/
	//private static final String REPLACEMENT_JIRA = "(0$3 * 5 * 5) + (0$7 * 5) + 0$11";
	//private static final String REPLACEMENT_JIRA = "(0$2 * 5 * 5) + (0$10 * 5) + 0$18";
	//private static final String REPLACEMENT_JIRA = "(0$2 * 5 * 5) + (0$7 * 5) + 0$12";
	static final String REPLACEMENT_JIRA = "(0$2 * 5 * 5) + (0$10 * 5) + 0$18 + 0$26/60 + 0$34/3600";
	private ColumnValueParameters regexpForValues = new ColumnValueParameters(REGEXP_JIRA, REPLACEMENT_JIRA);
	
	@BeforeClass
	public static void beforeClass() {
		BasicConfigurator.configure();
	}
	@Test
	public void testPivotSumWithJiraValue1() {
		List<String> values = new ArrayList<String>();
		values.add("1 week, 1 day, 1 hour");
		String result = Utils.calculatePivotValue(values, PivotTable.PARAM_TYPE_OF_CALCULATION_VALUE_SUM, regexpForValues, null, null);
		// 1*5*5 + 1*5 + 1
		assertEquals("", "31", result);
	}

	@Test
	public void testPivotAverageWithJiraValue1() {
		List<String> values = new ArrayList<String>();
		values.add("1 week, 1 day, 1 hour");
		values.add("1 week, 1 day, 2 hour");
		String result = Utils.calculatePivotValue(values, PivotTable.PARAM_TYPE_OF_CALCULATION_VALUE_AVERAGE, regexpForValues, null, null);
		// 1*5*5 + 1*5 + 1
		assertEquals("", "31.5", result);
	}
	
	@Test
	public void testPivotSumWithJiraValue10() {
		List<String> values = new ArrayList<String>();
		values.add("2 hours");
		String result = Utils.calculatePivotValue(values, PivotTable.PARAM_TYPE_OF_CALCULATION_VALUE_SUM, regexpForValues, null, null);
		// 1*5*5 + 1*5 + 1
		assertEquals("", "2", result);
	}

	@Test
	public void testPivotSumWithJiraValue11() {
		List<String> values = new ArrayList<String>();
		values.add("1w 1d 1h");
		String result = Utils.calculatePivotValue(values, PivotTable.PARAM_TYPE_OF_CALCULATION_VALUE_SUM, regexpForValues, null, null);
		// 1*5*5 + 1*5 + 1
		assertEquals("", "31", result);
	}

	@Test
	public void testPivotSumWithJiraValue12() {
		List<String> values = new ArrayList<String>();
		values.add("1 w 1 d 1 h");
		String result = Utils.calculatePivotValue(values, PivotTable.PARAM_TYPE_OF_CALCULATION_VALUE_SUM, regexpForValues, null, null);
		// 1*5*5 + 1*5 + 1
		assertEquals("", "31", result);
	}

	@Test
	public void testPivotSumWithJiraValue13() {
		List<String> values = new ArrayList<String>();
		values.add("1w, 1d, 1h");
		String result = Utils.calculatePivotValue(values, PivotTable.PARAM_TYPE_OF_CALCULATION_VALUE_SUM, regexpForValues, null, null);
		// 1*5*5 + 1*5 + 1
		assertEquals("", "31", result);
	}
	

	@Test
	public void testPivotSumWithJiraValue14() {
		List<String> values = new ArrayList<String>();
		values.add("1w, 1d, 1h, 1m, 30s");
		String result = Utils.calculatePivotValue(values, PivotTable.PARAM_TYPE_OF_CALCULATION_VALUE_SUM, regexpForValues, null, null);
		assertEquals("", "31.025", result);
	}	

	@Test
	public void testPivotSumWithJiraValue15() {
		List<String> values = new ArrayList<String>();
		values.add("12.51w 13.65d, 14.78h, 18.54m, 30.90s");
		values.add("12,51w");
		String result = Utils.calculatePivotValue(values, PivotTable.PARAM_TYPE_OF_CALCULATION_VALUE_SUM, regexpForValues, null, null);
		assertEquals("", "1671.0977", result);
	}	
	
	@Test
	public void testPivotSumWithJiraValue16() {
		List<String> values = new ArrayList<String>();
		//values.add("12.51w 13.65d, 14.78h, 18.54m, 30.90s");
		values.add("12.51w");
		String result = Utils.calculatePivotValue(values, PivotTable.PARAM_TYPE_OF_CALCULATION_VALUE_SUM, regexpForValues, null, null);
		assertEquals("", "312.75", result);
	}
	
	@Test
	public void testPivotSumWithJiraValue2() {
		List<String> values = new ArrayList<String>();
		values.add("2 weeks, 5 days, 6 hours");
		values.add("2 weeks, 5 days, 6 hours");
		values.add("1 week, 1 day, 1 hour");
		values.add("1 week");
		values.add("1 hour");
		String result = Utils.calculatePivotValue(values, PivotTable.PARAM_TYPE_OF_CALCULATION_VALUE_SUM, regexpForValues, null, null);
		assertEquals("", "219", result);
	}
	
	@Test
	public void testPivotAverageWithJiraValue2() {
		List<String> values = new ArrayList<String>();
		values.add("2 weeks, 5 days, 6 hours");
		values.add("2 weeks, 5 days, 6 hours");
		values.add("1 week, 1 day, 1 hour");
		values.add("1 week");
		values.add("1 hour");
		String result = Utils.calculatePivotValue(values, PivotTable.PARAM_TYPE_OF_CALCULATION_VALUE_AVERAGE, regexpForValues, null, null);
		assertEquals("", "43.8", result);
	}
	
	
	@Test
	public void testPivotCountWithJiraValue2() {
		List<String> values = new ArrayList<String>();
		values.add("2 weeks, 5 days, 6 hours");
		values.add("2 weeks, 5 days, 6 hours");
		values.add("1 week, 1 day, 1 hour");
		values.add("1 week");
		values.add("1 hour");
		String result = Utils.calculatePivotValue(values, PivotTable.PARAM_TYPE_OF_CALCULATION_VALUE_COUNT, regexpForValues, null, null);
		assertEquals("", "5", result);
	}
	
	@Test
	public void testPivotSumWithJiraValue3() {
		List<String> values = new ArrayList<String>();
		values.add("1 hour");
		String result = Utils.calculatePivotValue(values, PivotTable.PARAM_TYPE_OF_CALCULATION_VALUE_SUM, regexpForValues, null, null);
		// 1*5*5 + 1*5 + 1
		assertEquals("", "1", result);
	}
}
