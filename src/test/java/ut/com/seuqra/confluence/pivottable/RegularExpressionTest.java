package ut.com.seuqra.confluence.pivottable;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.script.ScriptException;

import com.udojava.evalex.Expression;

public class RegularExpressionTest {

	private Object evaluate (String expression) {
		Expression exp = new Expression(expression);
		return exp.eval();
	}
	
	public void testReplaceJiraDurationByFormula() throws ScriptException {
		// Regexp
		Pattern p = Pattern.compile(UtilsTest.REGEXP_JIRA);
		match(p, "1 week, 1 day, 1 hour", UtilsTest.REPLACEMENT_JIRA);
		match(p, "1 week, 2 days, 3 hours", UtilsTest.REPLACEMENT_JIRA);
		match(p, "2 weeks, 2 days, 3 hours", UtilsTest.REPLACEMENT_JIRA);
		match(p, "2 days, 3 hours", UtilsTest.REPLACEMENT_JIRA);
		match(p, "1 week, 2 days", UtilsTest.REPLACEMENT_JIRA);
		match(p, "1 week, 3 hours", UtilsTest.REPLACEMENT_JIRA);
		match(p, "1 week", UtilsTest.REPLACEMENT_JIRA);
		match(p, "2 weeks", UtilsTest.REPLACEMENT_JIRA);
		match(p, "1 day", UtilsTest.REPLACEMENT_JIRA);
		match(p, "2 days", UtilsTest.REPLACEMENT_JIRA);
		match(p, "1 hour", UtilsTest.REPLACEMENT_JIRA);
		match(p, "3 hours", UtilsTest.REPLACEMENT_JIRA);
		match(p, "5w 2d 3h", UtilsTest.REPLACEMENT_JIRA);
		match(p, "11h 40m", UtilsTest.REPLACEMENT_JIRA);
		match(p, "5w 2d 3h 4m 50s", UtilsTest.REPLACEMENT_JIRA);
		match(p, "5w 2.3d 3.6h 4m 50s", UtilsTest.REPLACEMENT_JIRA);
		String replacement = "ROUND("+UtilsTest.REPLACEMENT_JIRA+",0)";
		match(p, "1 week, 1 day, 1 hour", replacement);
		match(p, "12.51w 13.65d, 14.78h, 18.54m, 30.90s", replacement);
		match(p, "1h 58m", replacement);
		match(p, "3h", replacement);
		match(p, "3.5h", replacement);
		match(p, "60.45h", replacement);
	}

	private void match(Pattern p, String toMatch, String replacement) throws ScriptException {
		
		Matcher m = p.matcher(toMatch);
		// launch the search on all the occurrences
		boolean b = m.matches();
		// If it matches
		if(b) {
		    // For each group
		    for(int i=0; i <= m.groupCount(); i++) {
		        System.out.println("$" + i + " = " + m.group(i));
		    }
		} else {
			System.out.println("NO MATCH");
		}
		
		//String formula = toMatch.replaceFirst(p.pattern(),"(0$2 * 5 * 5) + (0$10 * 5) + 0$18");
		String formula = toMatch.replaceFirst(p.pattern(),	replacement) ;
		System.out.println(toMatch+" replaced by " + formula + " = " +evaluate(formula));
	}
	
	public void testRemoveDollarFromPrice() throws ScriptException {
		Pattern p = Pattern.compile("(\\$)(.*)");
		match(p, "$ 25", "$2");
		match(p, "$25", "$2");
		match(p, "$25.567", "$2");
	}
	
	public void testDayAndHour() throws ScriptException {
		String regexp = "(\\d*)(d) (\\d*)(h)";
		Pattern p = Pattern.compile(regexp);
		String replacement = "(0$1*8 + $3)";
		match(p, "3d 5h", replacement);
	}
	
	public void testDecimalDayAndHour() throws ScriptException {
		String regexp = UtilsTest.REGEXP('d') + UtilsTest.REGEXP('h');
		Pattern p = Pattern.compile(regexp);
		String replacement = "(0$2*8 + 0$10)";
		match(p, "3d", replacement);
		match(p, "3.5d", replacement);
		match(p, "4h", replacement);
		match(p, "4.5h", replacement);
		match(p, "5d 4.5h", replacement);
		match(p, "6.3d 4.5h", replacement);
	}
	
	public void testHour() throws ScriptException {
		String regexp = "(.*)(h)";
		Pattern p = Pattern.compile(regexp);
		String replacement = "$1";
		match(p, "3h", replacement);
		match(p, "3.5h", replacement);
		match(p, "60.45h", replacement);
	}
}
