package ut.com.seuqra.confluence.pivottable;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;

import java.util.List;
import java.util.Map;

import org.jsoup.nodes.Element;
import org.jsoup.parser.Parser;
import org.jsoup.parser.Tag;
import org.junit.Test;

import com.atlassian.sal.api.message.I18nResolver;
import com.seuqra.confluence.pivottable.ColumnParameters;
import com.seuqra.confluence.pivottable.ColumnValueParameters;
import com.seuqra.confluence.pivottable.Utils;

public class PivotTableTest {
	
	private static final String LABEL1_REG_EXP1_COLOR1_LABEL2_REG_EXP2_COLOR2 = "Label1/RegExp1/Color1,Label2/RegExp2/Color2";

	private ColumnParameters initColumnParameters(String params) {
		I18nResolver i18nResolver= mock(I18nResolver.class);
		return new ColumnParameters(params, i18nResolver);
	}
	
	@Test
	public void testMapInitializationWithLabels() {
		Map<String, List<String>> all = initColumnParameters(LABEL1_REG_EXP1_COLOR1_LABEL2_REG_EXP2_COLOR2).initMapWithEmptyValues();
		assertEquals(2, all.size());
	}

	@Test
	public void testMapInitializationWithNullLabels() {
		Map<String, List<String>> all = initColumnParameters(null).initMapWithEmptyValues();
		assertEquals(0, all.size());
	}

	@Test
	public void testPopulateColumnParameters() {
		ColumnValueParameters[] result = initColumnParameters(LABEL1_REG_EXP1_COLOR1_LABEL2_REG_EXP2_COLOR2).getColumnValuesParameters();
		assertEquals(2, result.length);
		assertEquals(result[0].getReplacement(), "Label1");
		assertEquals(result[0].getRegexp(), "RegExp1");
		assertEquals(result[0].getColor(), "Color1");
		assertEquals(result[1].getReplacement(), "Label2");
		assertEquals(result[1].getRegexp(), "RegExp2");
		assertEquals(result[1].getColor(), "Color2");
	}
	
	@Test
	public void testPopulateLabelsAndRegExpsWithOneParameter() {
		ColumnValueParameters[] result = initColumnParameters("Label1,Label2").getColumnValuesParameters();
		assertEquals(2, result.length);
		assertEquals(result[0].getReplacement(), "Label1");
		assertEquals(result[0].getRegexp(), "Label1");
		assertEquals(result[1].getReplacement(), "Label2");
		assertEquals(result[1].getRegexp(), "Label2");
	}
	
	@Test
	public void testPopulateLabelsAndRegExpsWithNullParam() {
		ColumnValueParameters[] result = initColumnParameters(null).getColumnValuesParameters();
		assertNull(result);
	}
	
	@Test
	public void testGetColumParametersForPatternMatching() {
		ColumnValueParameters columParameters = initColumnParameters(null).getColumnParametersForPatternMatching("RegExp1");
		assertEquals("RegExp1",columParameters.getReplacement());

		ColumnParameters labelsAndRegExps = initColumnParameters(LABEL1_REG_EXP1_COLOR1_LABEL2_REG_EXP2_COLOR2);
		columParameters = labelsAndRegExps.getColumnParametersForPatternMatching("RegExp1");
		assertEquals("Label1",columParameters.getReplacement());
		columParameters = labelsAndRegExps.getColumnParametersForPatternMatching("RegExp3");
		assertNull(columParameters);
	}

	@Test
	public void testGetColumParametersForPatternMatchingWithNullParam() {
		ColumnValueParameters columParameters = initColumnParameters(null).getColumnParametersForPatternMatching("RegExp1");
		assertEquals("RegExp1",columParameters.getReplacement());
	}

	@Test
	public void testGetColumParametersForPatternMatchingWithoutMatching() {
		ColumnParameters labelsAndRegExps = initColumnParameters(LABEL1_REG_EXP1_COLOR1_LABEL2_REG_EXP2_COLOR2);
		ColumnValueParameters columParameters = labelsAndRegExps.getColumnParametersForPatternMatching("RegExp3");
		assertNull(columParameters);
	}
	
	private Element getHTMLTable() {
		return Parser.parse("<html><head></head><body><table><tbody><tr><th>Column1</th><th>Column2</th></tr><tr><td>Value1</td><td>Value2</td></tr></table></html></body>", "");
	}
	
	@Test
	public void testColumnPosition() {
		int position = Utils.columnPosition("Column2", getHTMLTable());
		assertEquals(1, position);
	}
	
	@Test
	public void testColumnPositionWithWrongColumn() {
		int position = Utils.columnPosition("Column5", getHTMLTable());
		assertEquals(-1, position);
	}
	
	@Test
	public void testColumnPositionWithEmptyBody() {
		int position = Utils.columnPosition("Column1", null);
		assertEquals(-1, position);
	}
	
	@Test
	public void testColumnPositionWithNullParameter() {
		int position = Utils.columnPosition(null, null);
		assertEquals(-1, position);
	}
	
	@Test
	public void testAddColorInSpanTag() {
		Element td = new Element(Tag.valueOf("td"), "");
		String color = "green";
		td.text("testAddColor");
		Utils.addColorInSpanTag(td, color);
		String expected = "<td><span style=\"background-color: "+color+"; "+Utils.BACKGROUND_STYLE+"\">testAddColor</span></td>";
		assertEquals(expected, td.toString());
	}
	
	//private String body = "<table class=\"confluenceTable\"><tr class=\"confluenceTr\"><th class=\"confluenceTh\">Column1</th><th class=\"confluenceTh\">Column2</th><th class=\"confluenceTh\">Column3</th></tr><tr><td class=\"confluenceTd\">Column1Value1</td><td class=\"confluenceTd\">Column2Value1</td><td class=\"confluenceTd\">Column3Value1</td></tr><tr><td class=\"confluenceTd\">Column1Value2</td><td class=\"confluenceTd\">Column2Value2</td><td class=\"confluenceTd\">Column3Value3</td></tr><tr><td class=\"confluenceTd\">Column1Value1</td><td class=\"confluenceTd\">Column2Value3</td><td class=\"confluenceTd\">Column3Value3</td></tr></table>";

}
