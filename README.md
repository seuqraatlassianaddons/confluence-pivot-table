# **Pivot Table** plugin for Confluence

The **Pivot Table** macro allows you to display a pivot table based on tabular data

More information on Confluence Wiki: [Pivot Table Macro](https://seuqra.atlassian.net/wiki/display/PTM)

## Integration tests

1. Start a Confluence instance: `atlas-run`
1. Then run the integration tests: `atlas-integration-test --no-webapp --http-port 1990`